#include <Windows.h>
#include <cassert>
#include <CommCtrl.h>
#include <algorithm>
#include <libreg/SyscallWrapper.h>
#include "Win32Utils.h"
#include "res/Resource.h"
#include "MainWindow.h"
#include "Tabs/StartupTab.h"
#include "Tabs/ContainerTab.h"
#include "Tabs/MiscTab.h"
#include "Tabs/ContextMenuTab.h"
#include "Tabs/ContextMenuSendToTab.h"
#include "Tabs/ScheduledTasksTab.h"
#include "Tabs/ContextMenuNewTab.h"
#include "Layout.h"

// Use recent CommCtrl version
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

using namespace std::placeholders;
using libreg::Syscall;
using libreg::SyscallWithExpectedReturn;

using Win32Utils::Width;
using Win32Utils::Height;

MainWindow::MainWindow(HINSTANCE instance) : _hinstance(instance)
{
}

void MainWindow::PrepareEventHandlers()
{
  _router.Add({ WM_CLOSE }, std::bind(&MainWindow::OnClose, this));
  _router.Add({ WM_SIZE }, Win32MessageRouter::MessageHandlerRoutine{ std::bind(&MainWindow::OnWindowResize, this, _1, _2, _3, _4) });
  _router.Add({ WM_SIZING }, Win32MessageRouter::MessageHandlerRoutine{ std::bind(&MainWindow::OnWindowResizing, this, _1, _2, _3, _4) });
  _router.AddNotificationHandler({ TCN_SELCHANGE, GetControl(IDC_MAINTAB) }, std::bind(&MainWindow::OnTabChanged, this, _1));
  _router.Add(
    { WM_CTLCOLORSTATIC, {}, {}, reinterpret_cast<LPARAM>(GetControl(IDC_CONSOLE)) },
    Win32MessageRouter::MessageHandlerRoutine{ std::bind(&MainWindow::OnDrawConsole, this, _1, _2, _3, _4) });
}

void MainWindow::Refresh()
{
  Win32Utils::Log(_window, "Refresh all tabs");
  for (auto &e : _tabs)
  {
    Win32Utils::Log(_window, L" - Refresh tab: '" + e->Name() + L"'");
    e->Refresh();
  }
}

bool MainWindow::OnDrawConsole(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{
  auto error_flag = GetWindowLongPtr(GetControl(IDC_CONSOLE), GWLP_USERDATA);
  if (error_flag == 0)
  {
    return false;
  }

  // Show red background if error state is set
  SetBkColor(reinterpret_cast<HDC>(wparam), RGB(255, 0, 0));

  SetWindowLongPtr(window, DWLP_MSGRESULT, reinterpret_cast<LONG_PTR>(GetStockObject(WHITE_BRUSH)));

  return true;
}

void MainWindow::Show()
{
  SetProcessDPIAware(); // Required to avoid appearing 'blurry' on high res displays

  CreateDialogParam(
    _hinstance,
    MAKEINTRESOURCE(IDD_MAINWINDOW),
    nullptr,
    &MainWindow::WndProc,
    reinterpret_cast<LPARAM>(this));

  Syscall(
    RedrawWindow,
    _window,
    nullptr,
    nullptr,
    RDW_ERASE | RDW_INVALIDATE
  );

  SyscallWithExpectedReturn(
    ShowWindow,
    false,
    _window,
    SW_SHOW);
  
  Win32Utils::Log(_window, "WinToolSet 1.1.1.");
  Win32Utils::Log(_window, "Repo: https://bitbucket.org/_Blue/WinToolset");
  Win32Utils::Log(_window, "Build time : " + std::string(__DATE__) + ", " + std::string(__TIME__));
  Win32Utils::Log(_window, "MSC build version: " + std::to_string(_MSC_VER));

  Refresh();

  HandleMessages();
}

void MainWindow::OnClose()
{
  Syscall(DestroyWindow, _window);
  PostQuitMessage(0);
}

void MainWindow::HandleMessages()
{
  MSG msg = { 0 };
  while (GetMessage(&msg, nullptr, 0, 0))
  {
    if (msg.message == WM_KEYUP && msg.wParam == VK_F5)
    {
      Refresh(); // Ugly handling of F5 because Dispatchmessage won't call the WndProc
    }

    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
}

bool MainWindow::OnTabChanged(NMHDR& header)
{
  size_t tab = TabCtrl_GetCurSel(GetControl(IDC_MAINTAB));

  for (size_t i = 0; i < _tabs.size(); i++)
  {
    if (i == tab)
    {
      _tabs[i]->Show();
    }
    else
    {
      _tabs[i]->Hide();
    }
  }

  return true;
}

HWND MainWindow::GetControl(UINT id)
{
  assert(_window != nullptr);

  return GetDlgItem(_window, id);
}

void MainWindow::CreateTabs()
{
  static const std::vector<std::pair<std::wstring, std::wstring>> context_menu_types
  {
    {L"*", L"*"},
    {L"Folders", L"Folder"},
    {L"Directory", L"Directory"},
    {L"Desktop", L"desktopbackground"},
    {L"All objects", L"allfilesystemobjects"}
  };

  auto offset = ComputeTabOffset(); // Offset at which tabs should be drawn

  std::vector<std::unique_ptr<Tab>> context_menu_tabs;
  for (const auto& e : context_menu_types)
  {
    context_menu_tabs.emplace_back(std::make_unique<ContextMenuTab>(_window, offset.first, offset.second + 15, _router, e.first, e.second));
  }

  context_menu_tabs.emplace_back(std::make_unique<ContextMenuSendToTab>(_window, offset.first, offset.second + 15, _router));
  context_menu_tabs.emplace_back(std::make_unique<ContextMenuNewTab>(_window, offset.first, offset.second + 15, _router));
  context_menu_tabs.emplace_back(std::make_unique<ContextMenuTab>(_window, offset.first, offset.second + 15, _router, L"Custom"));

  _tabs.emplace_back(std::make_unique<StartupTab>(_window, offset.first, offset.second, _router));
  _tabs.emplace_back(std::make_unique<ScheduledTasksTab>(_window, offset.first, offset.second, _router, L"Scheduled tasks"));
  _tabs.emplace_back(std::make_unique<ContainerTab>(_window, offset.first, offset.second, _router, std::move(context_menu_tabs), L"Context menus"));
  _tabs.emplace_back(std::make_unique<MiscTab>(_window, offset.first, offset.second, _router));
}

void MainWindow::CreateUITabs()
{
  for (size_t i = 0; i < _tabs.size(); i++)
  {
    auto name = _tabs[i]->Name();

    TCITEM item = { 0 };
    item.pszText = const_cast<PTCHAR>(name.c_str());
    item.iImage = -1;
    item.mask = TCIF_TEXT;

    TabCtrl_InsertItem(GetControl(IDC_MAINTAB), i, &item);
    
    _tabs[i]->Initialize();
    if (i > 0)
    {
      _tabs[i]->Hide();
    }
  }
}

void MainWindow::PrepareResizeData()
{
  Syscall(
    EnumChildWindows,
    _window,
    reinterpret_cast<WNDENUMPROC>(&MainWindow::EnumWindowProc),
    reinterpret_cast<LPARAM>(this));
}

bool MainWindow::OnWindowResize(HWND, UINT, WPARAM, LPARAM)
{
  Syscall(
    GetWindowRect,
    _window,
    &_current_window_rect
  );

  Syscall(
    EnumChildWindows,
    _window,
    reinterpret_cast<WNDENUMPROC>(&MainWindow::ResizeWindowProc),
    reinterpret_cast<LPARAM>(this));

  return false;
}

bool MainWindow::OnWindowResizing(HWND, UINT, WPARAM, LPARAM lparam)
{
  RECT *rect = reinterpret_cast<RECT*>(lparam);

  if (Height(*rect) < Height(_original_window_rect))
  {
    rect->bottom = rect->top + Height(_original_window_rect);
  }
  if (Width(*rect) < Width(_original_window_rect))
  {
    rect->right = rect->left + Width(_original_window_rect);
  }

  return true;
}

std::pair<int, int> MainWindow::ComputeTabOffset()
{
  RECT rect = { 0 };
  Syscall(
    GetWindowRect,
    GetControl(IDC_MAINTAB),
    &rect
  );

  return { rect.left - _original_window_rect.left, rect.top - _original_window_rect.top};
}

BOOL MainWindow::EnumWindowProc(HWND hwnd, MainWindow* self)
{
  self->AddControl(hwnd);
  return true;
}

BOOL MainWindow::ResizeWindowProc(HWND hwnd, MainWindow* self)
{
  self->ResizeControl(hwnd);
  return true;
}

void MainWindow::AddControl(HWND hwnd)
{
  if (GetParent(hwnd) != _window)
  {
    return;
  }

  ControlInfos infos = { 0 };
  infos.hwnd = hwnd;

  // Get control position
  Syscall(GetWindowRect, hwnd, &infos.rect);

  // Map it to current window
  Syscall(MapWindowPoints, HWND_DESKTOP, _window, reinterpret_cast<POINT*>(&infos.rect), 2);

  auto it = layout::control_infos.find(GetDlgCtrlID(hwnd));
  assert(it != layout::control_infos.end());

  infos.left_stick = it->second & layout::stick_left;
  infos.right_stick = it->second & layout::stick_right;
  infos.top_stick = it->second & layout::stick_top;
  infos.bottom_stick = it->second & layout::stick_bottom;
  
  _controls.push_back(infos);
}

void MainWindow::ResizeControl(HWND control)
{
  if (GetParent(control) != _window)
  {
    return;
  }

  auto pred = [&control](const auto &e)
  {
    return e.hwnd == control;
  };

  auto it = std::find_if(_controls.begin(), _controls.end(), pred);
  assert(it != _controls.end());

  // Compute new position for control
  RECT new_rect = it->rect;

  auto x_diff = Width(_current_window_rect) - Width(_original_window_rect);
  auto y_diff = Height(_current_window_rect) - Height(_original_window_rect);

  if (!it->left_stick)
  {
    new_rect.left += x_diff;
  }

  if (!it->top_stick)
  {
    new_rect.top += y_diff;
  }

  if (it->right_stick)
  {
    new_rect.right += x_diff;
  }

  if (it->bottom_stick)
  {
    new_rect.bottom += y_diff;
  }

  Syscall(
    SetWindowPos,
    control,
    nullptr,
    new_rect.left,
    new_rect.top,
    Width(new_rect),
    Height(new_rect),
    SWP_NOZORDER | SWP_FRAMECHANGED | SWP_DRAWFRAME
  );
}

INT_PTR MainWindow::WndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_INITDIALOG)
  {
    SetWindowLongPtr(hwnd, GWLP_USERDATA, lparam);
    auto window = reinterpret_cast<MainWindow*>(lparam);
    window->_window = hwnd;
    
    Syscall(
      GetWindowRect,
      hwnd,
      &window->_original_window_rect
    );

    window->CreateTabs();
    window->CreateUITabs();
    window->PrepareResizeData();
    window->PrepareEventHandlers();

    return true;
  }

  MainWindow* window = reinterpret_cast<MainWindow*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
  if (window == nullptr)
  {
    return false; // Dialog hasn't been initialized yet
  }

  return window->_router.RouteMessage(hwnd, msg, wparam, lparam);
}

