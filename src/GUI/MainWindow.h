#pragma once

#include <Windows.h>
#include <vector>
#include "Tabs/Tab.h"
#include "Win32MessageRouter.h"

class MainWindow
{
public:
  MainWindow(HINSTANCE instance);

  void Show();

  struct ControlInfos
  {
    HWND hwnd;
    RECT rect;
    bool left_stick = false;
    bool right_stick = false;
    bool top_stick;
    bool bottom_stick;
  };

private:
  void HandleMessages();
  bool OnTabChanged(NMHDR&);
  void OnClose();
  bool OnWindowResize(HWND, UINT, WPARAM, LPARAM);
  bool OnWindowResizing(HWND, UINT, WPARAM, LPARAM);
  HWND GetControl(UINT id);
  void CreateTabs();
  void CreateUITabs();
  void PrepareResizeData();
  void PrepareEventHandlers();
  std::pair<int, int> ComputeTabOffset();
  void AddControl(HWND control);
  void ResizeControl(HWND control);
  void Refresh();
  bool OnDrawConsole(HWND window, UINT message, WPARAM wparam, LPARAM lparam);

  static INT_PTR CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
  static BOOL CALLBACK EnumWindowProc(HWND hwnd, MainWindow* self);
  static BOOL CALLBACK ResizeWindowProc(HWND hwnd, MainWindow* self);


  HINSTANCE _hinstance = nullptr;
  HWND _window;
  std::vector<std::unique_ptr<Tab>> _tabs;
  Win32MessageRouter _router;
  std::vector<ControlInfos> _controls;
  RECT _original_window_rect = { 0 };
  RECT _current_window_rect = { 0 };
};