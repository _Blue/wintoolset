#pragma once

#include <map>
#include "res/Resource.h"

namespace layout
{
  inline int stick_left = 1;
  inline int stick_right = 2;
  inline int stick_top = 4;
  inline int stick_bottom = 8;

  static inline const std::map<unsigned int, int> control_infos =
  {
    {IDC_CONSOLE, stick_left | stick_right | stick_bottom},
    {IDC_SEARCH, stick_left | stick_top},
    {IDC_CONTEXT_EXTENSION, stick_left | stick_top },
    {IDC_CREATE, stick_top | stick_right},
    {IDC_LIST, stick_right | stick_left | stick_bottom | stick_top},
    {IDC_MAINTAB, stick_right | stick_left | stick_bottom| stick_top},
    {IDC_CONTAINER_TAB, stick_right | stick_left | stick_bottom| stick_top},
    {IDC_MISC_HDDCHECK, stick_left | stick_top},
    {IDC_MISC_NTFSLASTACCESS, stick_left | stick_top},
    {IDC_MISC_NTFS8DOT3, stick_left | stick_top},
    {IDC_MISC_EXPLORERSHOW, stick_left | stick_top},
    {IDC_MISC_LEGACYBOOT, stick_left | stick_top},
    {IDC_MISC_VERBOSESTATUS, stick_left | stick_top},
    {IDC_MISC_CFG, stick_left | stick_top},
    {IDC_MISC_HDDCHECK, stick_left | stick_top},
  };
}
