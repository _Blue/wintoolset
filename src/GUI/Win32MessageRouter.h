#pragma once

#include <Windows.h>
#include <optional>
#include <vector>
#include <functional>

class Win32MessageRouter
{
public:
  struct MessageFilter
  {
    MessageFilter(
      std::optional<UINT> msg,
      std::optional<HWND> hwnd = {},
      std::optional<WPARAM> wparam = {},
      std::optional<LPARAM> lparam = {})
      : hwnd(hwnd), msg(msg), wparam(wparam), lparam(lparam)
    {
    }

    std::optional<HWND> hwnd;
    std::optional<UINT> msg;
    std::optional<WPARAM> wparam;
    std::optional<LPARAM> lparam;
  };
  
  struct NotificationFilter
  {
    NotificationFilter(
      std::optional<DWORD> message,
      std::optional<HWND> hwnd_from = {},
      std::optional<UINT> id_from = {})
      : message(message), hwnd_from(hwnd_from), id_from(id_from)
    {
    }
    std::optional<DWORD> message;
    std::optional<HWND> hwnd_from;
    std::optional<UINT> id_from;
  };

  struct CommandFilter
  {
    CommandFilter(
      std::optional<UINT> command,
      std::optional<UINT> code = {},
      std::optional<LPARAM> lparam = {}
      ) : command(command), code(code), lparam(lparam)
    {
    }

    CommandFilter(
      std::optional<UINT> command,
      std::optional<UINT> code,
      HWND lparam) : CommandFilter(command, code, reinterpret_cast<LPARAM>(lparam))
    {
    }

    std::optional<UINT> command;
    std::optional<UINT> code;
    std::optional<LPARAM> lparam;
  };

  using MessageFilterRoutine = std::function<bool(HWND, UINT, WPARAM, LPARAM)>;
  using MessageHandlerRoutine = std::function<bool(HWND, UINT, WPARAM, LPARAM)>;
  using NotificationFilterRoutine = std::function<bool(const NMHDR&)>;
  using NotificationHandlerRoutine = std::function<bool(NMHDR&)>;
  using CommandHandlerRoutine = std::function<bool()>;

  Win32MessageRouter() = default;

  BOOL RouteMessage(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

  void Add(const MessageFilter& filter, const MessageHandlerRoutine& routine);
  void Add(const MessageFilter& filter, const std::function<void()>& routine);
  void Add(const MessageFilterRoutine& filter, const MessageHandlerRoutine& routine);
  void AddNotificationHandler(const NotificationFilter& filter, const NotificationHandlerRoutine& routine);
  void AddNotificationHandler(const NotificationFilterRoutine& filter, const NotificationHandlerRoutine& routine);
  void AddCommandHandler(const CommandFilter& filter, const CommandHandlerRoutine& routine);

  void Reset();

private:

  std::vector<std::pair<MessageFilterRoutine, MessageHandlerRoutine>> _handlers;
};