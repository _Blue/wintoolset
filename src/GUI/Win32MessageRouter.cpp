#include <algorithm>
#include "Win32MessageRouter.h"
#include <CommCtrl.h>

BOOL Win32MessageRouter::RouteMessage(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  auto pred = [&](const auto& e)
  {
    return e.first(hwnd, msg, wparam, lparam);
  };

  auto handler = std::find_if(_handlers.begin(), _handlers.end(), pred);
  if (handler == _handlers.end())
  {
    return false;
  }
  else
  {
    return handler->second(hwnd, msg, wparam, lparam);
  }
}

void Win32MessageRouter::Add(const MessageFilter& filter, const MessageHandlerRoutine & routine)
{
  auto pred = [filter](HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
    if (filter.hwnd.value_or(hwnd) != hwnd)
    {
      return false;
    }
    else if (filter.msg.value_or(msg) != msg)
    {
      return false;
    }
    else if (filter.wparam.value_or(wparam) != wparam)
    {
      return false;
    }

    return filter.lparam.value_or(lparam) == lparam;
  };

  Add(pred, routine);
}

void Win32MessageRouter::Add(const MessageFilter& filter, const std::function<void()>& routine)
{
  auto handler = [routine](HWND, UINT, LPARAM, WPARAM)
  {
    routine();
    return true;
  };

  Add(filter, handler);
}

void Win32MessageRouter::Add(const MessageFilterRoutine& filter, const MessageHandlerRoutine& routine)
{
  _handlers.emplace_back(filter, routine);
}

void Win32MessageRouter::AddNotificationHandler(const NotificationFilter& filter, const NotificationHandlerRoutine& routine)
{
  auto pred = [filter](const NMHDR& notification)
  {
    if (filter.hwnd_from.value_or(notification.hwndFrom) != notification.hwndFrom)
    {
      return false;
    }
    else if (filter.id_from.value_or(notification.idFrom) != notification.idFrom)
    {
      return false;
    }

    return filter.message.value_or(notification.code) == notification.code;
  };

  AddNotificationHandler(pred, routine);
}

void Win32MessageRouter::AddNotificationHandler(const NotificationFilterRoutine& filter, const NotificationHandlerRoutine& routine)
{
  auto pred = [filter](HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
    if (msg != WM_NOTIFY)
    {
      return false;
    }

    return filter(*reinterpret_cast<NMHDR*>(lparam));
  };

  auto routine_wrapper = [routine](HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
    return routine(*reinterpret_cast<NMHDR*>(lparam));
  };

  return Add(pred, routine_wrapper);
}

void Win32MessageRouter::AddCommandHandler(const CommandFilter& filter, const CommandHandlerRoutine& routine)
{
  auto pred = [filter](HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
    if (msg != WM_COMMAND)
    {
      return false;
    }

    if (filter.command.value_or(LOWORD(wparam)) != LOWORD(wparam))
    {
      return false;
    }

    if (filter.code.value_or(HIWORD(wparam)) != HIWORD(wparam))
    {
      return false;
    }

    return filter.lparam.value_or(lparam) == lparam;
  };

  auto routine_wrapper = [routine](HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
    return routine();
  };

  return Add(pred, routine_wrapper);
}

void Win32MessageRouter::Reset()
{
  _handlers.clear();
}
