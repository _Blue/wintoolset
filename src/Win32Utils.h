#include <libreg/Key.h>
#include <libreg/Key.h>
#include <optional>

#pragma once
namespace Win32Utils
{
  struct Wow64RedirectionRAII
  {
    Wow64RedirectionRAII(bool change);
    Wow64RedirectionRAII(const Wow64RedirectionRAII&) = delete;
    Wow64RedirectionRAII(Wow64RedirectionRAII&&);

    const Wow64RedirectionRAII& operator=(const Wow64RedirectionRAII&) = delete;
    const Wow64RedirectionRAII& operator=(Wow64RedirectionRAII&&);

    ~Wow64RedirectionRAII();

  private:
    PVOID _value = nullptr;
    bool _change = false;
  };

  std::wstring ReadShortcutPath(const std::wstring& file);
  void CreateShortcut(const std::wstring& path, const std::wstring& target);
  void InitializeCom();
  std::wstring GetWindowText(HWND window);
  std::wstring Convert(const std::string& input);
  std::string Convert(const std::wstring& input);
  std::wstring GetCSIDLPath(int csidl);
  std::optional<std::wstring> BrowseForExistingFile(
    HWND parent,
    const std::wstring& description);

  int Width(const RECT& rect);
  int Height(const RECT& rect);

  void LogImpl(HWND window, const std::wstring& text);

  void OpenRegedit(const libreg::Key& path);
  bool IEquals(const std::string& left, const std::string& right);

  std::pair<libreg::Handle<HANDLE>, libreg::Handle<HANDLE>> CreatePipe(bool rs, bool ws);
  FILE* FileFromHandle(HANDLE  hFile, const char* mode);

  std::pair<std::string, std::string> CheckRun(const std::string& cmd);

  std::string GetErrorMessage(DWORD code);
  std::wstring GetCLSIDDisplayName(const std::wstring& clsid);
  
  void Log(HWND window, const std::string& text, bool lf = true);
  void Log(HWND window, const std::wstring& text, bool lf = true);
  void SetErrorState(HWND window, bool set = true);
  void LogError(HWND window, const std::string& prefix, const std::exception& error);
};

