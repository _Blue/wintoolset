//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WinToolset.rc
//
#define IDC_MYICON                      2
#define IDD_WINTOOLSET_DIALOG           102
#define IDI_WINTOOLSET                  107
#define IDI_SMALL                       108
#define IDC_WINTOOLSET                  109
#define IDR_MAINFRAME                   128
#define IDD_MAINWINDOW                  129
#define IDD_BASELIST                    130
#define IDD_STARTUP_CREATE              132
#define IDD_CONTAINERTAB                133
#define IDD_CONTEXT_CREATE              134
#define IDD_CONTEXT_SENDTO_CREATE       135
#define IDD_MISC_TAB                    136
#define IDD_CONTEXT_NEW_CREATE          137
#define IDC_MAINTAB                     1000
#define IDC_LIST                        1001
#define IDC_SEARCH                      1002
#define IDC_CREATE                      1003
#define IDC_STARTUP_CREATE_NAME         1004
#define IDC_CONTEXT_CREATE_DISPLAY_NAME 1005
#define IDC_STARTUP_CREATE_CMD          1006
#define IDC_STARTUP_CREATE_BROWSE       1007
#define IDC_STARTUP_ADD_TYPE            1008
#define IDC_CONSOLE                     1009
#define IDC_CONTAINER_TAB               1010
#define IDC_CONTEXT_CREATE_NAME         1011
#define IDC_CONTEXT_CREATE_CMD          1012
#define IDC_CONTEXT_EXTENSION           1012
#define IDC_CONTEXT_CREATE_BROWSE       1013
#define IDC_CONTEXT_CREATE_TYPE         1014
#define IDC_MISC_HDDCHECK               1015
#define IDC_MISC_NTFSLASTACCESS         1016
#define IDC_CONTEXT_NEW_CREATE_EXTENSION 1016
#define IDC_MISC_NTFS8DOT3              1017
#define IDC_MISC_EXPLORERSHOW           1018
#define IDC_MISC_LEGACYBOOT             1019
#define IDC_MISC_VERBOSESTATUS          1020
#define IDC_MISC_CFG                    1021
#define IDR_LISTMENU                    1100
#define ID_LIST_OPEN                    1101
#define ID_LIST_REMOVE                  1102
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
