#pragma once

#include <taskschd.h>
#include "ListTab.h"


class ScheduledTasksTab : public ListTab
{
public:
  ScheduledTasksTab(
    HWND window,
    int x_offset,
    int y_offset,
    Win32MessageRouter& router,
    const std::wstring& name);

protected:
  virtual std::unique_ptr<IController> GetController() override;
  virtual void InitializeNewDialog(HWND dialog, Win32MessageRouter& router) override;
  virtual void Initialize() override;

private:
  std::vector<TASK_TRIGGER_TYPE2> _types;
};