#include <Windows.h>
#include <Windowsx.h>

#include "ContextMenuNewTab.h"
#include "Controllers/ContextMenuNewController.h"
#include "Win32Utils.h"
#include "res/Resource.h"


using namespace libreg;
using libreg::Syscall;

ContextMenuNewTab::ContextMenuNewTab(
  HWND main_window,
  int x_offset,
  int y_offset,
  Win32MessageRouter& router)
  : ListTab(main_window, x_offset, y_offset, L"New", router, { L"Extension", L"Flags"}, IDD_CONTEXT_NEW_CREATE)
{
  PermanentlyHideControl(IDC_CONTEXT_EXTENSION);
}

std::unique_ptr<IController> ContextMenuNewTab::GetController()
{
  return std::make_unique<ContextMenuNewController>(GetControl(IDC_LIST));
}

void ContextMenuNewTab::InitializeNewDialog(HWND dialog, Win32MessageRouter & router)
{
  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_CONTEXT_NEW_CREATE_EXTENSION), L"extension (ex: .txt)");

  router.AddCommandHandler({ {}, EN_CHANGE }, std::bind(&ContextMenuNewTab::OnEdited, this));
  router.AddCommandHandler({ IDOK }, std::bind(&ContextMenuNewTab::OnOk, this));
  router.AddCommandHandler({ IDCANCEL }, std::bind(&ContextMenuNewTab::OnCancel, this));
}

bool ContextMenuNewTab::OnOk()
{
  auto& controller = dynamic_cast<ContextMenuNewController&>(*_controller);
  auto extension = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_NEW_CREATE_EXTENSION));

  if (extension.empty() || extension.front() != '.')
  {
    MessageBox(_window, L"Extension must start by a '.'", L"Invalid input", MB_ICONWARNING | MB_OK);
    return false;
  }

  try
  {
    controller.CreateEntry(extension);
  }
  catch (const std::exception& e)
  {
    Win32Utils::LogError(_window, "Error while creating context menu (new) entry", e);
  }

  EndDialog(_dialog, 0);

  Refresh();
  return false;
}

bool ContextMenuNewTab::OnCancel()
{
  EndDialog(_dialog, 0);

  return false;
}

bool ContextMenuNewTab::OnEdited()
{
  Button_Enable(
    GetDlgItem(_dialog, IDOK),
    !Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_NEW_CREATE_EXTENSION)).empty());
  return false;
}
