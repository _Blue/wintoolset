#pragma once

#include <Windows.h>
#include <map>
#include "GUI/Win32MessageRouter.h"

class Tab
{
public:
  Tab(HWND main_window,  DWORD dialog_id, int x_offset, int y_offset, const std::wstring& name, Win32MessageRouter& router);

  virtual void Initialize() = 0;
  virtual void Refresh() = 0;
  const std::wstring& Name() const;

  virtual void Show();
  virtual void Hide();

  const std::map<UINT, HWND> Controls() const;

protected:
  HWND GetControl(UINT id);
  void PermanentlyHideControl(UINT id);
  Win32MessageRouter& _router;
  HWND _window = nullptr;
  int _x_offset = 0;
  int _y_offset = 0;

private:
  void PrepareControls(HWND dialog);
  void AddControl(HWND control);
  bool RouteMessage(HWND hwnd, UINT msg, WPARAM wparam, LPARAM);

  static INT_PTR CALLBACK DlgProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
  static BOOL CALLBACK EnumWindowProc(HWND hwnd, Tab* self);

  HWND _dialog = nullptr;
  DWORD _dialog_id = 0;
  std::wstring _name;
  std::map<UINT, HWND> _controls;
};