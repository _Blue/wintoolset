#pragma once

#include <memory>
#include "Tab.h"
#include "Controllers/IController.h"

class ListTab : public Tab
{
public:
  ListTab(
    HWND main_window,
    int x_offset,
    int y_offset,
    const std::wstring& name,
    Win32MessageRouter& router,
    const std::vector<std::wstring>& columns,
    USHORT new_dialog_id);

  virtual void Initialize() override;

  virtual void Refresh() override;

protected:
  virtual std::unique_ptr<IController> GetController() = 0;
  virtual void InitializeNewDialog(HWND dialog, Win32MessageRouter& router) = 0;

  HWND _dialog = nullptr;
  std::unique_ptr<IController> _controller;

private:
  bool OnGetDisplayInfo(NMHDR& header);
  void AddColumn(const std::wstring& column, size_t pos);
  bool OnContextMenuRequest(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
  bool OnOpen();
  bool OnRemove();
  bool OnFilterEdited();
  bool DisplayNewDialog();

  static INT_PTR CALLBACK DialogProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

  std::vector<std::wstring> _columns;
  HMENU _menu = nullptr;
  USHORT _new_dialog_id = 0;
  Win32MessageRouter _dialog_router;
};