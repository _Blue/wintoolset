#include <Shlobj.h>
#include <windowsx.h>
#include <sstream>

#include "StartupTab.h"
#include "Controllers/StartupController.h"
#include "Win32Utils.h"
#include "res/Resource.h"

struct Entry
{
  std::wstring name;
  std::variant<std::pair<libreg::Hive, libreg::Wow64Access>, int> location;
};

static const std::vector<Entry> entry_types = 
{
  {L"Registry: HKLM (X86)", {std::make_pair(libreg::Hive::LocalMachine, libreg::Wow64Access::Key32Bits)}},
  {L"Registry: HKLM (X64)", {std::make_pair(libreg::Hive::LocalMachine, libreg::Wow64Access::Key64Bits)}},
  {L"Registry: HKCU (X86)", {std::make_pair(libreg::Hive::CurrentUser, libreg::Wow64Access::Key32Bits)}},
  {L"Registry: HKCU (X64)", {std::make_pair(libreg::Hive::CurrentUser, libreg::Wow64Access::Key64Bits)}},
  {L"Registry: HKU (X86)", {std::make_pair(libreg::Hive::CurrentUser, libreg::Wow64Access::Key32Bits)}},
  {L"Registry: HKU (X64)", {std::make_pair(libreg::Hive::CurrentUser, libreg::Wow64Access::Key64Bits)}},
  {L"Startup folder: current user", {CSIDL_STARTUP}},
  {L"Startup folder: all users", {CSIDL_COMMON_STARTUP}}
};

StartupTab::StartupTab(HWND window, int x_offset, int y_offset, Win32MessageRouter& router)
  : ListTab(window, x_offset, y_offset, L"Startup", router, { L"Name", L"Location", L"Command" }, IDD_STARTUP_CREATE)
{
  PermanentlyHideControl(IDC_CONTEXT_EXTENSION);
}

std::unique_ptr<IController> StartupTab::GetController()
{
  return std::make_unique<StartupController>(GetControl(IDC_LIST));
}

void StartupTab::InitializeNewDialog(HWND dialog, Win32MessageRouter & router)
{
  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_STARTUP_CREATE_NAME), L"Name");
  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_STARTUP_CREATE_CMD), L"Command");

  for (const auto& e : entry_types)
  {
    ComboBox_AddString(GetDlgItem(dialog, IDC_STARTUP_ADD_TYPE), e.name.c_str());
  }

  ComboBox_SetCurSel(GetDlgItem(dialog, IDC_STARTUP_ADD_TYPE), 0);

  router.AddCommandHandler({ IDC_STARTUP_CREATE_NAME}, std::bind(&StartupTab::OnNameEdited, this));
  router.AddCommandHandler({ IDOK }, std::bind(&StartupTab::OnOk, this));
  router.AddCommandHandler({ IDCANCEL }, std::bind(&StartupTab::OnCancel, this));
  router.AddCommandHandler({ IDC_STARTUP_CREATE_BROWSE }, std::bind(&StartupTab::OnBrowse, this));
}

bool StartupTab::OnOk()
{
  auto controller = dynamic_cast<StartupController&>(*_controller);
  auto name = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_STARTUP_CREATE_NAME));
  auto cmd = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_STARTUP_CREATE_CMD));

  const auto& type = entry_types[ComboBox_GetCurSel(GetDlgItem(_dialog, IDC_STARTUP_ADD_TYPE))];

  try
  {
    if (type.location.index() == 0)
    {
      auto location = std::get<std::pair<libreg::Hive, libreg::Wow64Access>>(type.location);

      controller.CreateRegistryEntry(location.first, location.second, name, cmd);
    }
    else
    {
      assert(type.location.index() == 1);

      controller.CreateStatupEntry(std::get<int>(type.location), name, cmd);
    }
  }
  catch (const std::exception& e)
  {
    std::stringstream str;
    str << "Error while creating startup entry: " << e.what();

    Win32Utils::Log(_window, str.str());
    Win32Utils::SetErrorState(_window);
  }

  EndDialog(_dialog, 0);

  Refresh();

  return false;
}

bool StartupTab::OnCancel()
{
  EndDialog(_dialog, 0);

  return false;
}

bool StartupTab::OnNameEdited()
{
  auto text = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_STARTUP_CREATE_NAME));
  
  Button_Enable(GetDlgItem(_dialog, IDOK),!text.empty());

  return false;
}

bool StartupTab::OnBrowse()
{
  auto path = Win32Utils::BrowseForExistingFile(_window, L"Select a program to execute");
  
  if (path.has_value())
  {
    SetWindowText(GetDlgItem(_dialog, IDC_STARTUP_CREATE_CMD), path.value().c_str());
  }

  return false;
}