#pragma once

#include <Windows.h>
#include <map>
#include "GUI/Win32MessageRouter.h"
#include "Tab.h"

class ContainerTab : public Tab
{
public:
  ContainerTab(
    HWND main_window,
    int x_offset,
    int y_offset,
    Win32MessageRouter& router,
    std::vector<std::unique_ptr<Tab>>&& tabs,
    const std::wstring& name);

  virtual void Refresh() override;
  virtual void Initialize() override;
  virtual void Show() override;
  virtual void Hide() override;

private:
  bool OnTabChanged(NMHDR& header);
  void AdjustControls();
  void AdjustControl(HWND control);
  static BOOL CALLBACK EnumWindowProc(HWND hwnd, ContainerTab* self);

  std::vector<std::unique_ptr<Tab>> _tabs;
};