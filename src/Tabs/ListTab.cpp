#include <Windows.h>
#include <CommCtrl.h>
#include <windowsx.h>
#include <functional>
#include <libreg/SyscallWrapper.h>

#include "res/Resource.h"
#include "ListTab.h"
#include "Win32Utils.h"

using libreg::Syscall;
using libreg::SyscallWithExpectedReturn;
using namespace std::placeholders;

ListTab::ListTab(
  HWND main_window,
  int x_offset,
  int y_offset,
  const std::wstring& name,
  Win32MessageRouter& router,
  const std::vector<std::wstring> &columns,
  USHORT new_dialog_id)
  : Tab(main_window, IDD_BASELIST, x_offset, y_offset, name, router), 
  _columns(columns), 
  _menu(GetSubMenu(LoadMenu(nullptr, MAKEINTRESOURCE(IDR_LISTMENU)), 0)),
  _new_dialog_id(new_dialog_id)
{
}

void ListTab::Initialize()
{
  _controller = GetController();

  Edit_SetCueBannerText(GetControl(IDC_SEARCH), L"Filter");

  // Set list view flags
  DWORD style = ListView_GetExtendedListViewStyle(GetControl(IDC_LIST));
  ListView_SetExtendedListViewStyle(GetControl(IDC_LIST), style | LVS_EX_FULLROWSELECT);

  _router.AddNotificationHandler({ LVN_GETDISPINFO, GetControl(IDC_LIST) }, std::bind(&ListTab::OnGetDisplayInfo, this, _1));
  _router.AddNotificationHandler({ NM_DBLCLK, GetControl(IDC_LIST) }, std::bind(&ListTab::OnOpen, this));
  _router.AddCommandHandler({ IDC_CREATE, BN_CLICKED, GetControl(IDC_CREATE) }, std::bind(&ListTab::DisplayNewDialog, this));
  _router.AddCommandHandler({ {}, EN_CHANGE, GetControl(IDC_SEARCH) }, std::bind(&ListTab::OnFilterEdited, this));

  _router.Add(
    { WM_CONTEXTMENU, {}, reinterpret_cast<WPARAM>(GetControl(IDC_LIST)) }, // Control HWND is passed as wparam
    Win32MessageRouter::MessageFilterRoutine{ std::bind(&ListTab::OnContextMenuRequest, this, _1, _2, _3, _4) });

  for (size_t i = 0; i < _columns.size(); i++)
  {
    AddColumn(_columns[i], i);
  }
}

void ListTab::Refresh()
{
  try
  {
    _controller->Refresh(Win32Utils::GetWindowTextW(GetControl(IDC_SEARCH)));
  }
  catch (const std::exception& e)
  {
    std::stringstream str;
    str << "Error while refreshing tab \"" << Win32Utils::Convert(Name()) << "\"" << e.what() << std::endl;

    Win32Utils::LogError(_window, str.str(), e);
  }

  ListView_SetItemCount(GetControl(IDC_LIST), _controller->Rows());

  for (size_t i = 0; i < _columns.size(); i++)
  {
    ListView_SetColumnWidth(GetControl(IDC_LIST), i, LVSCW_AUTOSIZE);
  }
}

bool ListTab::OnGetDisplayInfo(NMHDR& notification)
{
  NMLVDISPINFO *display = reinterpret_cast<NMLVDISPINFO*>(&notification);

  if (!(display->item.mask & LVIF_TEXT) || static_cast<size_t>(display->item.iItem) >= _controller->Rows())
  {
    return false;
  }

  lstrcpynW(display->item.pszText, _controller->GetItem(display->item.iItem, display->item.iSubItem).c_str(), display->item.cchTextMax);

  return true;
}

void ListTab::AddColumn(const std::wstring& name, size_t pos)
{
  LVCOLUMN column = { 0 };

  column.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_SUBITEM;
  column.cchTextMax = static_cast<int>(name.size() + 1);
  column.pszText = const_cast<wchar_t*>(name.data());
  column.cx = 120;
  ListView_InsertColumn(GetControl(IDC_LIST), pos, &column);
}

bool ListTab::OnContextMenuRequest(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (_controller->GetSelectedRows().empty())
  {
    return false; // Don't display menu if no row is selected
  }

  auto selection = TrackPopupMenu(
    _menu,
    TPM_TOPALIGN | TPM_LEFTALIGN | TPM_RETURNCMD,
    GET_X_LPARAM(lparam),
    GET_Y_LPARAM(lparam),
    0,
    _window,
    nullptr);

  if (selection == 0)
  {
    return true;
  }

  if (selection == ID_LIST_REMOVE)
  {
    OnRemove();
  }
  else if (selection == ID_LIST_OPEN)
  {
    OnOpen();
  }

  return true;
}

bool ListTab::OnOpen()
{
  try
  {
    _controller->OnOpen();
  }
  catch (const std::exception& e)
  {
    Win32Utils::LogError(_window, "Failed to open entry", e);
  }

  return true;
}

bool ListTab::OnRemove()
{
  try
  {
    _controller->OnDelete();
    ListView_SetItemState(GetControl(IDC_LIST), -1, LVIF_STATE, LVIS_SELECTED);
  }
  catch (const std::exception& e)
  {
    Win32Utils::LogError(_window, "Failed to remove entry", e);
  }

  Refresh();
  return true;
}

bool ListTab::OnFilterEdited()
{
  Refresh();
  return true;
}

bool ListTab::DisplayNewDialog()
{
  DialogBoxParam(
    nullptr,
    MAKEINTRESOURCE(_new_dialog_id),
    _window,
    &ListTab::DialogProc,
    reinterpret_cast<LPARAM>(this));

  _dialog = nullptr;
  _dialog_router.Reset();

  Refresh();

  return true;
}

INT_PTR ListTab::DialogProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_INITDIALOG)
  {
    auto* tab = reinterpret_cast<ListTab*>(lparam);
    tab->_dialog = hwnd;
    SetWindowLongPtr(hwnd, GWLP_USERDATA, lparam);
    tab->InitializeNewDialog(hwnd, tab->_dialog_router);

    return true;
  }

  auto* tab = reinterpret_cast<ListTab*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
  if (tab == nullptr)
  {
    return false;
  }

  return tab->_dialog_router.RouteMessage(hwnd, msg, wparam, lparam);
}