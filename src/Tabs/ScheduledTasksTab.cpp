#include <windowsx.h>
#include <sstream>

#include "ScheduledTasksTab.h"
#include "Controllers/StartupController.h"
#include "Win32Utils.h"
#include "res/Resource.h"
#include "Controllers/ScheduledTasksController.h"


ScheduledTasksTab::ScheduledTasksTab(
  HWND window,
  int x_offset,
  int y_offset,
  Win32MessageRouter& router,
  const std::wstring &name)
  : ListTab(
    window,
    x_offset,
    y_offset,
    name,
    router,
    { L"Name", L"Path", L"Command", L"Triggers" }, 0)
{
  PermanentlyHideControl(IDC_CONTEXT_EXTENSION);
}

std::unique_ptr<IController> ScheduledTasksTab::GetController()
{
  return std::make_unique<ScheduledTasksController>(GetControl(IDC_LIST));
}

void ScheduledTasksTab::InitializeNewDialog(HWND dialog, Win32MessageRouter & router)
{
}

void ScheduledTasksTab::Initialize()
{
  ListTab::Initialize();
  PermanentlyHideControl(IDC_CREATE);
}
