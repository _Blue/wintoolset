#pragma once

#include <libreg/Key.h>
#include "ListTab.h"

class ContextMenuNewTab : public ListTab
{
public:
  ContextMenuNewTab(
    HWND main_window,
    int x_offset,
    int y_offset,
    Win32MessageRouter& router);

private:
  virtual std::unique_ptr<IController> GetController() override;
  virtual void InitializeNewDialog(HWND dialog, Win32MessageRouter& router) override;

  bool OnOk();
  bool OnCancel();
  bool OnEdited();
};