#include <windowsx.h>

#include "ContextMenuSendToTab.h"
#include "Controllers/ContextMenuSendToController.h"
#include "Win32Utils.h"
#include "res/Resource.h"

using libreg::Syscall;

ContextMenuSendToTab::ContextMenuSendToTab(HWND main_window, int x_offset, int y_offset, Win32MessageRouter& router)
  : ListTab(main_window, x_offset, y_offset, L"Send to", router, { L"Name", L"Type", L"Target" }, IDD_CONTEXT_SENDTO_CREATE)
{
}

std::unique_ptr<IController> ContextMenuSendToTab::GetController()
{
  return std::make_unique<ContextMenuSendToController>(GetControl(IDC_LIST));
}

void ContextMenuSendToTab::InitializeNewDialog(HWND dialog, Win32MessageRouter & router)
{
  SetWindowLongPtr(dialog, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_CONTEXT_CREATE_DISPLAY_NAME), L"Display name");
  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_CONTEXT_CREATE_NAME), L"Item name");
  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_CONTEXT_CREATE_CMD), L"Target");

  for (const auto& e : ContextMenuSendToController::Types())
  {
    ComboBox_AddString(GetDlgItem(dialog, IDC_CONTEXT_CREATE_TYPE), std::get<1>(e).c_str());
  }

  ComboBox_SetCurSel(GetDlgItem(dialog, IDC_CONTEXT_CREATE_TYPE), 0);

  router.AddCommandHandler({ {}, EN_CHANGE }, std::bind(&ContextMenuSendToTab::OnEdited, this));
  router.AddCommandHandler({ IDOK }, std::bind(&ContextMenuSendToTab::OnOk, this));
  router.AddCommandHandler({ IDCANCEL }, std::bind(&ContextMenuSendToTab::OnCancel, this));
  router.AddCommandHandler({ IDC_CONTEXT_CREATE_BROWSE }, std::bind(&ContextMenuSendToTab::OnBrowse, this));
  router.AddCommandHandler({ {}, CBN_SELCHANGE }, std::bind(&ContextMenuSendToTab::OnComboChanged, this));
}

void ContextMenuSendToTab::Initialize()
{
  ListTab::Initialize();

  PermanentlyHideControl(IDC_CONTEXT_EXTENSION);
}

bool ContextMenuSendToTab::OnOk()
{
  auto& controller = dynamic_cast<ContextMenuSendToController&>(*_controller);
  
  controller.CreateEntry(
    Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_NAME)),
    ComboBox_GetCurSel(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_TYPE)),
    Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_CMD)));

  EndDialog(_dialog, 0);

  Refresh();

  return true;
}

bool ContextMenuSendToTab::OnCancel()
{
  EndDialog(_dialog, 0);
  return true;
}

bool ContextMenuSendToTab::OnEdited()
{
  auto text = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_NAME));

  Button_Enable(GetDlgItem(_dialog, IDOK), !text.empty());
  
  return false;
}

bool ContextMenuSendToTab::OnBrowse()
{
  auto path = Win32Utils::BrowseForExistingFile(_window, L"Select a program or folder");

  if (path.has_value())
  {
    SetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_CMD), path.value().c_str());
  }

  return true;
}

bool ContextMenuSendToTab::OnComboChanged()
{
  size_t index = ComboBox_GetCurSel(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_TYPE));
  assert(index < ContextMenuSendToController::Types().size());

  const auto& entry = ContextMenuSendToController::Types()[index];

  Syscall(
    SetWindowText,
    GetDlgItem(_dialog, IDC_CONTEXT_CREATE_CMD),
    std::get<2>(entry).c_str()
  );

  return true;
}