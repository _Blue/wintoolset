#include <Windows.h>
#include <Windowsx.h>
#include <shldisp.h>
#include <Shlobj.h>
#include <sstream>
#include <ole2.h>
#include <atlbase.h>
#include <atlcom.h>
#include <libreg/SyscallWrapper.h>

#include "ContextMenuTab.h"
#include "Controllers/ContextMenuController.h"
#include "Win32Utils.h"
#include "res/Resource.h"


using libreg::Syscall;

ContextMenuTab::ContextMenuTab(HWND main_window,
  int x_offset,
  int y_offset,
  Win32MessageRouter& router,
  const std::wstring& name,
  const std::wstring& extension)
  : ContextMenuTab(main_window, x_offset, y_offset, router, name)
{
  _extension = extension;
}

ContextMenuTab::ContextMenuTab(HWND main_window,
  int x_offset,
  int y_offset,
  Win32MessageRouter& router,
  const std::wstring& name)
  : ListTab(main_window, x_offset, y_offset, name, router, { L"Type", L"Location", L"Value" }, IDD_CONTEXT_CREATE)
{
}

void ContextMenuTab::Initialize()
{
  ListTab::Initialize();

  if (_extension.has_value()) 
  {
    PermanentlyHideControl(IDC_CONTEXT_EXTENSION);
    return;
  }

  // Only applies to 'custom' tab
  ConfigureCompletion();
  Edit_SetCueBannerText(GetControl(IDC_CONTEXT_EXTENSION), L"Extension (ex: .txt)");

  _router.AddCommandHandler(
    { {}, EN_CHANGE, GetControl(IDC_CONTEXT_EXTENSION) },
    std::bind(&ContextMenuTab::OnExtensionEdited, this));

  EnableEdit(false);
}

void ContextMenuTab::EnableEdit(bool enable)
{
  EnableWindow(GetControl(IDC_LIST), enable);
  EnableWindow(GetControl(IDC_SEARCH), enable);
  EnableWindow(GetControl(IDC_CREATE), enable);
}

void ContextMenuTab::ConfigureCompletion()
{
  typedef CComEnum<IEnumString, &IID_IEnumString, LPOLESTR, _Copy<LPOLESTR> > CComEnumString;

  Win32Utils::InitializeCom();
  InitializeExtensionList();

  CComPtr<IAutoComplete2> auto_complete(nullptr);
  auto_complete.CoCreateInstance(CLSID_AutoComplete);
  
  CComObject<CComEnumString> *enum_string;
  CComObject<CComEnumString>::CreateInstance(&enum_string);

  CComPtr<IEnumString> enum_string_instance(enum_string);

  enum_string->Init(
    _available_extensions_ptr.data(),
    _available_extensions_ptr.data() + _available_extensions_ptr.size(),
    nullptr);

  auto_complete->Init(GetControl(IDC_CONTEXT_EXTENSION), enum_string_instance, nullptr, nullptr);
  auto_complete->SetOptions(ACO_AUTOSUGGEST | ACO_UPDOWNKEYDROPSLIST);
}

void ContextMenuTab::InitializeExtensionList()
{
  auto sub_keys = libreg::Key::Open(libreg::Hive::Root, "", libreg::Access::EnumerateSubKey).SubKeys();

  auto pred = [](const auto& e)
  {
    return e.Name().Value().empty() || e.Name().Value().front() != '.';
  };

  // Keep extensions only
  sub_keys.erase(std::remove_if(sub_keys.begin(), sub_keys.end(), pred), sub_keys.end());

  auto routine = [](const auto &e)
  {
    return e.Name().Value();
  };
  std::transform(
    sub_keys.begin(),
    sub_keys.end(),
    std::back_inserter(_available_extensions),
    routine);
  
  auto ptr_routine = [](std::wstring& e)
  {
    return e.data();
  };


  std::transform(
    _available_extensions.begin(),
    _available_extensions.end(),
    std::back_inserter(_available_extensions_ptr),
    ptr_routine);
}

std::unique_ptr<IController> ContextMenuTab::GetController()
{
  auto controller = std::make_unique<ContextMenuController>(GetControl(IDC_LIST));
  if (_extension.has_value())
  {
    controller->SetRoot(_extension.value());
  }

  return controller;
}

void ContextMenuTab::InitializeNewDialog(HWND dialog, Win32MessageRouter & router)
{
  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_CONTEXT_CREATE_DISPLAY_NAME), L"Display name");
  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_CONTEXT_CREATE_NAME), L"Item name");
  Edit_SetCueBannerText(GetDlgItem(dialog, IDC_CONTEXT_CREATE_CMD), L"Command");

  router.AddCommandHandler({ {}, EN_CHANGE }, std::bind(&ContextMenuTab::OnEdited, this));
  router.AddCommandHandler({ IDOK }, std::bind(&ContextMenuTab::OnOk, this));
  router.AddCommandHandler({ IDCANCEL }, std::bind(&ContextMenuTab::OnCancel, this));
  router.AddCommandHandler({ IDC_CONTEXT_CREATE_BROWSE }, std::bind(&ContextMenuTab::OnBrowse, this));
}

bool ContextMenuTab::OnOk()
{
  auto& controller = dynamic_cast<ContextMenuController&>(*_controller);
  auto name = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_NAME));
  auto display_name = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_DISPLAY_NAME));
  auto cmd = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_CMD));

  try
  {
    controller.CreateEntry(name, display_name, cmd);
  }
  catch (const std::exception& e)
  {
    Win32Utils::LogError(_window, "Error while creating context menu entry", e);
  }

  EndDialog(_dialog, 0);

  Refresh();
  return false;
}

bool ContextMenuTab::OnCancel()
{
  EndDialog(_dialog, 0);

  return false;
}

bool ContextMenuTab::OnEdited()
{
  auto text = Win32Utils::GetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_NAME));

  Button_Enable(GetDlgItem(_dialog, IDOK), !text.empty());

  return true;
}

bool ContextMenuTab::OnBrowse()
{
  auto path = Win32Utils::BrowseForExistingFile(_window, L"Select a program to execute");

  if (path.has_value())
  {
    SetWindowText(GetDlgItem(_dialog, IDC_CONTEXT_CREATE_CMD), path.value().c_str());
  }

  return false;
}

bool ContextMenuTab::OnExtensionEdited()
{
  auto& contoller = dynamic_cast<ContextMenuController&>(*_controller);

  auto extension = Win32Utils::GetWindowText(GetControl(IDC_CONTEXT_EXTENSION));

  bool valid = !extension.empty() && contoller.SetRoot(extension);
  EnableEdit(valid);

  if (valid)
  {
    Refresh();
  }
  return true;
}