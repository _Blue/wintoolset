#pragma once

#include "ListTab.h"

class StartupTab : public ListTab
{
public:
  StartupTab(HWND window, int x_offset, int y_offset, Win32MessageRouter& router);

protected:
  virtual std::unique_ptr<IController> GetController() override;
  virtual void InitializeNewDialog(HWND dialog, Win32MessageRouter& router) override;

private:
  bool OnOk();
  bool OnCancel();
  bool OnNameEdited();
  bool OnBrowse();
};