#include <sstream>
#include <windows.h>
#include <windowsx.h>
#include "MiscTab.h"
#include "res/Resource.h"
#include "Controllers/MiscController.h"
#include "Win32Utils.h"

using namespace MiscController;

MiscTab::MiscTab(HWND main_window, int x_offset, int y_offset, Win32MessageRouter& router)
  : Tab(main_window, IDD_MISC_TAB, x_offset, y_offset, L"Misc", router)
{
}

void MiscTab::Initialize()
{
  _features = std::map <UINT, std::pair<RefreshRoutine, UpdateRoutine>>
  {
    {IDC_MISC_HDDCHECK, {HDDCheckEnabled, EnableHDDCheck}},
    {IDC_MISC_NTFSLASTACCESS, {NTFSLastAccessEnabled, EnableNTFSLastAccess}},
    {IDC_MISC_NTFS8DOT3, {NTFS8Dot3Enabled, EnableNTFS8Dot3}},
    {IDC_MISC_EXPLORERSHOW, {ShowExplorerEnabled, EnableShowExplorer}},
    {IDC_MISC_LEGACYBOOT, {LegacyBootEnabled, EnableLegacyBoot}},
    {IDC_MISC_VERBOSESTATUS, {VerboseStatusEnabled, EnableVerboseStatus}},
    {IDC_MISC_CFG, {CFGEnabled, EnableCFG}}
  };

  for (const auto&e : _features)
  {
    auto handler = std::bind(&MiscTab::OnButtonChecked, this, e.first, e.second.second);
    _router.AddCommandHandler({ e.first }, handler);
  }
}

void MiscTab::Refresh()
{
  for (const auto&e : _features)
  {
    TryRefresh(e.first, e.second.first);
  }
}

bool MiscTab::IsCheckboxChecked(UINT control)
{
  return Button_GetCheck(GetControl(control));
}

bool MiscTab::OnButtonChecked(UINT control, const UpdateRoutine& update)
{
  TryToggle(control, update, IsCheckboxChecked(control));

  Refresh();

  return false;
}

void MiscTab::TryRefresh(UINT control, const RefreshRoutine& routine)
{
  auto hwnd = GetControl(control);

  try
  {
    Button_SetCheck(hwnd, routine());
    Button_Enable(hwnd, true);
  }
  catch (const std::exception& e)
  {
    auto name = Win32Utils::GetWindowText(hwnd);

    Win32Utils::LogError(_window, "Error while refreshing Misc entry: '" + Win32Utils::Convert(name) + "'", e);
    Button_Enable(hwnd, false);
  }
}

void MiscTab::TryToggle(UINT control, const UpdateRoutine& routine, bool toggle)
{
  auto hwnd = GetControl(control);

  try
  {
    routine(toggle);
  }
  catch (const std::exception& e)
  {
    auto name = Win32Utils::GetWindowText(hwnd);
    Win32Utils::LogError(_window, "Error while updating Misc entry: '" + Win32Utils::Convert(name) + "'", e);

    Button_Enable(hwnd, false);
  }
}
