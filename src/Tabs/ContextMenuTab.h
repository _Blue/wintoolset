#pragma once

#include "ListTab.h"

class ContextMenuTab : public ListTab
{
public:
  ContextMenuTab(
    HWND main_window,
    int x_offset,
    int y_offset,
    Win32MessageRouter& router,
    const std::wstring& name);

  ContextMenuTab(
    HWND main_window,
    int x_offset,
    int y_offset,
    Win32MessageRouter& router,
    const std::wstring& name,
    const std::wstring& extension);

private:
  virtual std::unique_ptr<IController> GetController() override;
  virtual void InitializeNewDialog(HWND dialog, Win32MessageRouter& router) override;
  virtual void Initialize() override;

  void ConfigureCompletion();
  void InitializeExtensionList();
  bool OnOk();
  bool OnCancel();
  bool OnEdited();
  bool OnBrowse();
  bool OnExtensionEdited();
  void EnableEdit(bool enable);

  std::optional<std::wstring> _extension;
  std::vector<std::wstring> _available_extensions;
  std::vector<wchar_t*> _available_extensions_ptr;
};