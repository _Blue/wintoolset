#include <Windows.h>
#include <CommCtrl.h>
#include <libreg/SyscallWrapper.h>
#include <cassert>
#include "ContainerTab.h"
#include "res/Resource.h"
#include "GUI/Layout.h"
#include "Win32Utils.h"

using namespace std::placeholders;
using namespace libreg;

ContainerTab::ContainerTab(
  HWND main_window,
  int x_offset,
  int y_offset,
  Win32MessageRouter& router,
  std::vector<std::unique_ptr<Tab>>&& tabs,
  const std::wstring& name)
  : Tab(main_window, IDD_CONTAINERTAB, x_offset, y_offset, name, router),
  _tabs(std::move(tabs))
{
  for (size_t i = 0; i < _tabs.size(); i++)
  {
    auto name = _tabs[i]->Name();

    TCITEM item = { 0 };
    item.pszText = const_cast<PTCHAR>(name.c_str());
    item.iImage = -1;
    item.mask = TCIF_TEXT;

    TabCtrl_InsertItem(GetControl(IDC_CONTAINER_TAB), i, &item);

    _tabs[i]->Initialize();
  }

  AdjustControls();
}

void ContainerTab::Refresh()
{
  for (auto &e : _tabs)
  {
    Win32Utils::Log(_window, L"   - Refresh tab: '" + e->Name() + L"'");

    e->Refresh();
  }
}

void ContainerTab::Initialize()
{
  _router.AddNotificationHandler(
    { TCN_SELCHANGE, GetControl(IDC_CONTAINER_TAB) },
    std::bind(&ContainerTab::OnTabChanged, this, _1));
}

void ContainerTab::Show()
{
  Tab::Show();

  size_t tab = TabCtrl_GetCurSel(GetControl(IDC_CONTAINER_TAB));

  for (size_t i = 0; i < _tabs.size(); i++)
  {
    if (i == tab)
    {
      _tabs[i]->Show();
    }
    else
    {
      _tabs[i]->Hide();
    }
  }
}

void ContainerTab::Hide()
{
  Tab::Hide();

  for (const auto &e : _tabs)
  {
    e->Hide();
  }
}

bool ContainerTab::OnTabChanged(NMHDR& header)
{
  Show();
  return true;
}

void ContainerTab::AdjustControls()
{
  for (const auto& tab : _tabs)
  {
    for (const auto& e : tab->Controls())
    {
      AdjustControl(e.second);
    }
  }
}

void ContainerTab::AdjustControl(HWND control)
{
  if (GetParent(control) != _window)
  {
    return;
  }

  auto entry = layout::control_infos.find(GetDlgCtrlID(control));
  assert(entry != layout::control_infos.end());

  RECT rect = { 0 };
  Syscall(GetWindowRect, control, &rect);
  Syscall(MapWindowPoints, HWND_DESKTOP, _window, reinterpret_cast<POINT*>(&rect), 2);

  if (entry->second & layout::stick_right)
  {
      rect.right -= _x_offset;
      if (!(entry->second & layout::stick_left))
      {
        rect.left -= _x_offset;
      }
  }

  if (entry->second & layout::stick_bottom)
  {
    rect.bottom -= 30;
  }

  Syscall(
    SetWindowPos,
    control,
    nullptr,
    rect.left,
    rect.top,
    Win32Utils::Width(rect),
    Win32Utils::Height(rect),
    SWP_NOZORDER | SWP_FRAMECHANGED | SWP_DRAWFRAME
  );
}

BOOL ContainerTab::EnumWindowProc(HWND hwnd, ContainerTab* self)
{
  self->AdjustControl(hwnd);
  return true;
}
