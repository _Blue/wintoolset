#pragma once

#include "ListTab.h"

class ContextMenuSendToTab : public ListTab
{
public:
  ContextMenuSendToTab(
    HWND main_window,
    int x_offset,
    int y_offset,
    Win32MessageRouter& router);

private:
  virtual std::unique_ptr<IController> GetController() override;
  virtual void InitializeNewDialog(HWND dialog, Win32MessageRouter& router) override;
  virtual void Initialize() override;

  bool OnOk();
  bool OnCancel();
  bool OnEdited();
  bool OnBrowse();
  bool OnComboChanged();
};