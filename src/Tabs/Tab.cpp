#include <cassert>
#include <Windows.h>
#include <CommCtrl.h>

#include "Tab.h"
#include "libreg/SyscallWrapper.h"
#include "Win32Utils.h"

using libreg::Syscall;
using libreg::SyscallWithExpectedReturn;

Tab::Tab(HWND main_window, DWORD dialog_id, int x_offset, int y_offset, const std::wstring& name, Win32MessageRouter& router)
  : _window(main_window), _dialog_id(dialog_id), _x_offset(x_offset), _y_offset(y_offset), _name(name), _router(router)
{
  Syscall(CreateDialogParam,
    nullptr,
    MAKEINTRESOURCE(dialog_id),
    nullptr,
    &Tab::DlgProc,
    reinterpret_cast<LPARAM>(this));


  _dialog = nullptr;
}

const std::wstring& Tab::Name() const
{
  return _name;
}

void Tab::Show()
{
  for (auto e : _controls)
  {
    ShowWindow(e.second, SW_SHOW);
  }
}

void Tab::Hide()
{
  for (auto e : _controls)
  {
    ShowWindow(e.second, SW_HIDE);
  }
}

const std::map<UINT, HWND> Tab::Controls() const
{
  return _controls;
}

HWND Tab::GetControl(UINT id)
{
  auto it = _controls.find(id);
  assert(it != _controls.end());

  return it->second;
}

void Tab::PermanentlyHideControl(UINT id)
{
  auto it = _controls.find(id);
  assert(it != _controls.end());

  Syscall(ShowWindow, it->second, SW_HIDE);
  _controls.erase(it);
}

void Tab::PrepareControls(HWND dialog)
{
  _dialog = dialog; 

  SyscallWithExpectedReturn(
    EnumChildWindows,
    TRUE, 
    dialog,
    reinterpret_cast<WNDENUMPROC>(&Tab::EnumWindowProc),
    reinterpret_cast<LPARAM>(this));
}

INT_PTR Tab::DlgProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_INITDIALOG)
  {
    reinterpret_cast<Tab*>(lparam)->PrepareControls(hwnd);
    SetWindowLongPtr(hwnd, GWLP_USERDATA, lparam);
    
    return true;
  }

  Tab* tab = reinterpret_cast<Tab*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
  if (tab == nullptr)
  {
    return false;
  }
  
  return tab->RouteMessage(hwnd, msg, wparam, lparam);
}

BOOL Tab::EnumWindowProc(HWND hwnd, Tab* self)
{
  self->AddControl(hwnd);

  return true;
}

void Tab::AddControl(HWND control)
{
  if (GetParent(control) != _dialog)
  {
    return;
  }

  // Move the control from the dialog to the main window
  Syscall(SetParent, control, _window);

  // Add the control to the control list for this tab
  int id = GetDlgCtrlID(control);
  auto result = _controls.emplace(GetDlgCtrlID(control), control);
  assert(result.second); // Check that insertion occured

  // Compute window dimensions
  RECT window_rect = { 0 };
  Syscall(GetWindowRect, _window, &window_rect);


  auto _window_width = Win32Utils::Width(window_rect);
  auto _window_height = Win32Utils::Height(window_rect);

  // Get control position
  RECT rect = { 0 };

  // Get absolute rect
  Syscall(GetWindowRect, control, &rect);
  Syscall(MapWindowPoints, HWND_DESKTOP, _window, reinterpret_cast<POINT*>(&rect), 2);

  // Update position
  SyscallWithExpectedReturn(
    SetWindowPos,
    TRUE,
    control,
    nullptr,
    rect.left + _x_offset,
    rect.top + _y_offset,
    0,
    0,
    SWP_NOSIZE | SWP_NOZORDER);
}

bool Tab::RouteMessage(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (_dialog != nullptr) // If the dialog is being processed, don't route messages to the main window yet
  {
    return false;
  }

  return _router.RouteMessage(hwnd, msg, wparam, lparam);
}
