#pragma once 

#include "Tab.h"

class MiscTab : public Tab
{
public:
  MiscTab(
    HWND main_window,
    int x_offset,
    int y_offset,
    Win32MessageRouter& router);

  virtual void Initialize() override;

  virtual void Refresh() override;

  using RefreshRoutine = std::function<bool()>;
  using UpdateRoutine = std::function<void(bool)>;

private:
  bool IsCheckboxChecked(UINT control);
  bool OnButtonChecked(UINT control, const UpdateRoutine& update);

  void TryRefresh(UINT control, const RefreshRoutine& routine);
  void TryToggle(UINT control, const UpdateRoutine& routine, bool toggle);

  std::map<UINT, std::pair<RefreshRoutine, UpdateRoutine>> _features;
};