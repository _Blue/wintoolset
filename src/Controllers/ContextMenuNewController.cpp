#include <libreg/KeyNotFoundException.h>
#include <libreg/ValueNotFoundException.h>
#include "ContextMenuNewController.h"
#include "Win32Utils.h"

using namespace libreg;

ContextMenuNewController::ContextMenuNewController(HWND list)
  : ListController<ListEntry<libreg::Key>>(list)
{
}

void ContextMenuNewController::RefreshImpl()
{
  auto root = Key::Open(Hive::Root, "", Access::Read);
  
  for (const auto &e : root.SubKeys())
  {
    if (!e.Name().Value().empty() && e.Name().Value().front() == '.')
    {
      Visit(e.Name().Value(), e);
    }
  }
}

void ContextMenuNewController::CreateEntry(const std::wstring& extension)
{
  assert(!extension.empty() && extension.front() == '.');

  // Create extension key (.xxx)
  auto key = Key::OpenOrCreate(Hive::Root, extension, Access::AllAccess);

  // Assign to file class 
  CreateFileClassIfMissing(key);

  // Create shellnew key
  auto subkey = key.CreateSubKey("ShellNew");

  // NullFile is required (doesn't show up without it)
  subkey.SetValue("NullFile", "", ValueType::Sz);
}

void ContextMenuNewController::DeleteImpl(Entry& row)
{
  const auto& data = row.GetData();
  auto writable_key = Key::Open(data.Hive(), data.Path(), Access::AllAccess);

  writable_key.Delete();
}

void ContextMenuNewController::OpenImpl(Entry& row)
{
  Win32Utils::OpenRegedit(row.GetData());
}

size_t ContextMenuNewController::Columns() const
{
  return 2;
}

bool ContextMenuNewController::Visit(const std::wstring& filetype, const Key& key, size_t depth)
{
  auto shellnew = key.TryOpenSubKey("ShellNew", Access::Read);
  if (shellnew.has_value())
  {
    AddEntry(filetype, shellnew.value());
    return true;
  }

  if (depth > 0)
  {
    return false;
  }

  auto subkeys = key.SubKeys();
  return std::any_of(subkeys.begin(), subkeys.end(), [&](const auto& e) { return Visit(filetype, e, depth + 1); });
}

void ContextMenuNewController::AddEntry(const std::wstring& extension, const Key& key)
{
  auto config = key.TryOpenSubKey("Config", Access::Read);
  auto config_values = config.has_value() ? config.value().Values() : std::vector<std::pair<MultiString, ValueType>>{};

  auto shellnew_values = key.Values();
  std::copy(shellnew_values.begin(), shellnew_values.end(), std::back_inserter(config_values));

  std::wstringstream flags;
  bool first = true;
  for (const auto &e : config_values)
  {
    if (!first)
    {
      flags << ", ";
    }
    first = false;

    flags << e.first.Value();
  }

  _rows.emplace_back(Entry{key, {extension, flags.str()} });
}

void ContextMenuNewController::CreateFileClassIfMissing(libreg::Key& extension_key)
{
  bool valid = false;
  try
  {
    valid = extension_key.GetValue<MultiString>("", libreg::ValueType::Sz).Value().empty();
  }
  catch (const ValueNotFoundException&)
  {
    // Value does not exist -> class is missning
  }

  if (valid)
  {
    return;
  }
  
  auto extension = extension_key.Name().Value().substr(1);
  auto class_name = extension + L"file";
  auto file_title = extension + L" " + L"file";

  std::stringstream str;
  str << "Extension: '" << extension_key.Name() << "' has no associated class. Creating class: '";
  str << class_name << "'";

  Win32Utils::Log(GetParent(_list), str.str());

  // Create class
  auto class_key = Key::OpenOrCreate(Hive::Root, class_name, Access::AllAccess);

  // Set file title
  class_key.SetValue("", file_title, ValueType::Sz);

  // Assign extension to class
  extension_key.SetValue("", class_name, ValueType::Sz);
}
