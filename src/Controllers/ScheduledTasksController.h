#include "ListEntry.h"
#include "Controllers\ListController.h"
#include "ScheduledTask.h"

#pragma once

class ScheduledTasksController : public ListController<ListEntry<std::shared_ptr<ScheduledTask>>>
{
public:
  ScheduledTasksController(HWND list);
  ScheduledTasksController(const ScheduledTasksController&) = delete;

  const ScheduledTasksController& operator=(const ScheduledTasksController&) = delete;

  virtual void RefreshImpl() override;

protected:
  virtual void DeleteImpl(Entry& row) override;
  virtual void OpenImpl(Entry& row) override;
  virtual size_t Columns() const override;

private:
  void Visit(ITaskFolder* folder);
  void Visit(IRegisteredTaskCollection* tasks, ITaskFolder* folder);
  void Visit(ITaskFolderCollection* folders);
  void Visit(IRegisteredTask* task, ITaskFolder* parent);
  static std::wstring FormatTriggers(const std::vector<TASK_TRIGGER_TYPE2>& triggers);
};

