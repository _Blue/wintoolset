#pragma once

#include <Windows.h>
#include <utility>
#include <vector>
#include "IController.h"

template <typename TEntry> 
class ListController : public IController
{
public:
  using Entry = TEntry;

  ListController(HWND list);

  virtual void OnDelete() override;
  virtual void OnOpen() override;
  virtual const std::wstring& GetItem(size_t row, size_t column) const override;
  virtual std::vector<size_t> GetSelectedRows() const override;
  std::vector<Entry> GetSelectedEntries() const;

  virtual void Refresh(const std::wstring& filter) override;
  virtual size_t Rows() const override;
  

protected:
  virtual void DeleteImpl(Entry& row) = 0;
  virtual void OpenImpl(Entry& row) = 0;
  virtual void RefreshImpl() = 0;

  std::vector<Entry> _rows;
  HWND _list = nullptr;

};

#include "ListController.hxx"