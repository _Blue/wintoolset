#pragma once

#include <Windows.h>
#include <CommCtrl.h>
#include <shlwapi.h>
#include <algorithm>
#include <libreg/SyscallWrapper.h>
#include "ListController.h"

template<typename Entry>
inline ListController<Entry>::ListController(HWND list) : _list(list)
{
}

template<typename Entry>
inline void ListController<Entry>::OnDelete()
{
  auto entries = GetSelectedEntries();

  std::wstringstream message;
  message << "Delete the following entries: \r\n";

  for (const auto& e : entries)
  {
    message << "- " << e.GetEntry(0) << "\r\n";
  }
  message << "?";

  auto message_content = message.str();
  if (MessageBox(libreg::Syscall(GetParent, _list), message_content.c_str(), L"Delete entries ?", MB_OKCANCEL) != IDOK)
  {
    return;
  }

  for (auto& e : entries)
  {
    DeleteImpl(e);
  }
}

template<typename Entry>
inline void ListController<Entry>::OnOpen()
{
  for (auto& e : GetSelectedEntries())
  {
    OpenImpl(e);
  }
}

template<typename Entry>
inline const std::wstring& ListController<Entry>::GetItem(size_t row, size_t column) const
{
  return _rows[row].GetEntry(column);
}

template<typename TEntry>
inline std::vector<size_t> ListController<TEntry>::GetSelectedRows() const
{
  std::vector<size_t> entries;

  int row = ListView_GetNextItem(_list, -1, LVNI_SELECTED);
  while (row != -1)
  {
    entries.push_back(row);
    row = ListView_GetNextItem(_list, row, LVNI_SELECTED);
  }

  return entries;
}

template<typename Entry>
inline std::vector<Entry> ListController<Entry>::GetSelectedEntries() const
{
  auto routine = [this](size_t row)
  {
    return _rows[row];
  };

  auto rows = GetSelectedRows();

  std::vector<Entry> entries;

  std::transform(rows.begin(), rows.end(), std::back_inserter(entries), routine);

  return entries;
}

template<typename TEntry>
inline void ListController<TEntry>::Refresh(const std::wstring& filter)
{
  // Clear previous rows
  _rows.clear();

  // Collect new rows
  RefreshImpl();

  if (filter.empty())
  {
    return;
  }

  // Apply filter
  auto pred = [this, &filter](const TEntry& entry)
  {
    for (size_t i = 0; i < Columns(); i++)
    {
      if (StrStrI(entry.GetEntry(i).c_str(), filter.c_str()) != nullptr)
      {
        return false; // Keep if string is present
      }
    }

    return true; // Otherwirse delete
  };

  _rows.erase(std::remove_if(_rows.begin(), _rows.end(), pred), _rows.end());
}

template<typename TEntry>
inline size_t ListController<TEntry>::Rows() const
{
  return _rows.size();
}
