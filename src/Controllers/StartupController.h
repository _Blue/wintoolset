#include <libreg/Key.h>
#include <variant>

#include "ListEntry.h"
#include "Controllers\ListController.h"

#pragma once

class StartupController : public ListController<ListEntry<std::variant<std::pair<libreg::Key, std::wstring>, std::wstring>>>
{
public:
  StartupController(HWND list);

  virtual void RefreshImpl() override;

  void CreateRegistryEntry(libreg::Hive hive, libreg::Wow64Access wow64, const std::wstring& name, const std::wstring& cmd);
  void CreateStatupEntry(int csidl, const std::wstring& name, const std::wstring& cmd);

protected:
  virtual void DeleteImpl(Entry& row) override;
  virtual void OpenImpl(Entry& row) override;
  virtual size_t Columns() const override;

private:
  void CollectEntries(libreg::Hive hive, const std::wstring& path, bool wow64);
  void CollectEntries(int cidl, const std::wstring& type);
  void CreateEntry(const libreg::Key& key, const std::wstring& name, const std::wstring& cmd, libreg::Hive hive, bool wow64);

  static void OpenExplorer(const std::wstring& path);
  static void DeleteRegistryValue(libreg::Key& key, const std::wstring& value);
  static void DeleteStartupFolderEntry(const std::wstring& path);
};

