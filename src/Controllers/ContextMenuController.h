#include <libreg/Key.h>
#include <variant>
#include <functional>
#include <optional>
#include <map>

#include "ListEntry.h"
#include "Controllers\ListController.h"

#pragma once

class ContextMenuController : public ListController<ListEntry<libreg::Key>>
{
public:
  ContextMenuController(HWND list);
  ContextMenuController(const ContextMenuController&) = delete;

  const ContextMenuController& operator=(const ContextMenuController&) = delete;

  virtual void RefreshImpl() override;
  void CreateEntry(const std::wstring& name, const std::wstring& display_name, const std::wstring& cmd);
  bool SetRoot(const std::wstring& root);

protected:
  virtual void DeleteImpl(Entry& row) override;
  virtual void OpenImpl(Entry& row) override;
  virtual size_t Columns() const override;

private:
  void Visit(const std::wstring& path, const std::function<void(ContextMenuController*, const libreg::Key&)> &routine);
  void VisitShellEntry(const libreg::Key& key);
  void VisitShellExContextMenuEntry(const libreg::Key& key);
  void AddEntry(const libreg::Key& key, const std::wstring& value);

  std::optional<libreg::Key> _root;
};

