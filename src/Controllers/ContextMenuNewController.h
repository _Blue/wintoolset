#include <libreg/Key.h>

#include "ListEntry.h"
#include "Controllers\ListController.h"

#pragma once

class ContextMenuNewController : public ListController<ListEntry<libreg::Key>>
{
public:
  ContextMenuNewController(HWND list);
  ContextMenuNewController(const ContextMenuNewController&) = delete;

  const ContextMenuNewController& operator=(const ContextMenuNewController&) = delete;

  virtual void RefreshImpl() override;
  void CreateEntry(const std::wstring& extension);

protected:
  virtual void DeleteImpl(Entry& row) override;
  virtual void OpenImpl(Entry& row) override;
  virtual size_t Columns() const override;

private:
  bool Visit(const std::wstring& filetype, const libreg::Key& key, size_t depth = 0);
  void AddEntry(const std::wstring& extension, const libreg::Key& key);
  void CreateFileClassIfMissing(libreg::Key& extension_key);
 
};

