#pragma once

namespace MiscController
{
  bool HDDCheckEnabled();
  void EnableHDDCheck(bool enable);

  bool NTFSLastAccessEnabled();
  void EnableNTFSLastAccess(bool enable);

  bool NTFS8Dot3Enabled();
  void EnableNTFS8Dot3(bool enable);

  bool ShowExplorerEnabled();
  void EnableShowExplorer(bool enable);

  bool LegacyBootEnabled();
  void EnableLegacyBoot(bool enable);

  bool VerboseStatusEnabled();
  void EnableVerboseStatus(bool enable);

  bool CFGEnabled();
  void EnableCFG(bool enable);
};