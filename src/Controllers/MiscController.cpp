#include <algorithm>
#include <libreg/Key.h>
#include <libreg/SyscallWrapper.h>
#include <libreg/ValueNotFoundException.h>
#include "MiscController.h"
#include "Win32Utils.h"

using namespace libreg;
using Win32Utils::Wow64RedirectionRAII;

static DWORD ValueOrDefault(Key& key, const MultiString& name, DWORD default_value)
{
  try
  {
    return key.GetValue<DWORD>(name, ValueType::Dword);
  }
  catch (const ValueNotFoundException&)
  {
    return default_value;
  }
}

static void Clean(std::string& input)
{
  auto pred = [](char c)
  {
    return !isalpha(c);
  };

  input.erase(std::remove_if(input.begin(), input.end(), pred), input.end());
}

bool MiscController::HDDCheckEnabled()
{
  auto key = Key::Open(Hive::LocalMachine, L"SYSTEM\\CurrentControlSet\\Control\\Session Manager", Access::Read);

  return !key.GetValue<MultiString>("BootExecute", ValueType::None).Value().empty();
}

void MiscController::EnableHDDCheck(bool enable)
{
  auto key = Key::Open(Hive::LocalMachine, L"SYSTEM\\CurrentControlSet\\Control\\Session Manager", Access::SetValue);

  auto value = enable ? "Autocheck Autochk *" : "";
  
  key.SetValue("BootExecute", value, ValueType::Sz);
}

bool MiscController::NTFSLastAccessEnabled()
{
  auto key = Key::Open(Hive::LocalMachine, "SYSTEM\\CurrentControlSet\\Control\\FileSystem", Access::Read);

  return ValueOrDefault(key, "NtfsDisableLastAccessUpdate", 0) != 1;
}

void MiscController::EnableNTFSLastAccess(bool enable)
{
  auto key = Key::Open(Hive::LocalMachine, "SYSTEM\\CurrentControlSet\\Control\\FileSystem", Access::SetValue);

  key.SetValue("NtfsDisableLastAccessUpdate", !enable, ValueType::Dword);
}

bool MiscController::NTFS8Dot3Enabled()
{
  auto key = Key::Open(Hive::LocalMachine, L"SYSTEM\\CurrentControlSet\\Control\\FileSystem", Access::Read);

  return ValueOrDefault(key, "NtfsDisable8dot3NameCreation", 0) != 1;
}

void MiscController::EnableNTFS8Dot3(bool enable)
{
  auto key = Key::Open(Hive::LocalMachine, L"SYSTEM\\CurrentControlSet\\Control\\FileSystem", Access::SetValue);

  key.SetValue("NtfsDisable8dot3NameCreation", !enable, ValueType::Dword);
}

bool MiscController::ShowExplorerEnabled()
{
  auto key = Key::Open(Hive::CurrentUser, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced", Access::Read);

  return ValueOrDefault(key, "HideFileExt", 1) == 0
    && ValueOrDefault(key, "ShowSuperHidden", 0) == 1
    && ValueOrDefault(key, "Hidden", 0) == 1;
}

void MiscController::EnableShowExplorer(bool enable)
{
  auto key = Key::Open(Hive::CurrentUser, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced", Access::SetValue);
  key.SetValue("HideFileExt", !enable, ValueType::Dword);
  key.SetValue("ShowSuperHidden", enable, ValueType::Dword);
  key.SetValue("Hidden", enable, ValueType::Dword);
}

bool MiscController::LegacyBootEnabled()
{
#ifndef _M_X64
  Wow64RedirectionRAII wow64raii(true);
#endif

  auto output = Win32Utils::CheckRun("bcdedit /enum {default}");

  std::istringstream content(output.first);

  std::vector<std::string> matched_lines;
  for (std::string line; std::getline(content, line);)
  {
    if (line.find("bootmenupolicy") == 0)
    {
      matched_lines.emplace_back(line);
    }
  }

  if (matched_lines.size() != 1)
  {
    std::stringstream error;
    error << "Failed to find bootmenupolicy entry in bcdedit output.";
    error << " stdout: '" << output.first << "', stderr: '" << output.second << "'";

    throw std::runtime_error(error.str());
  }

  std::string& line = matched_lines[0];

  // Clean spaces and tabs
  Clean(line);

  return Win32Utils::IEquals(line, "bootmenupolicyLegacy");
}

void MiscController::EnableLegacyBoot(bool enable)
{
#ifndef _M_X64
  Wow64RedirectionRAII wow64raii(true);
#endif

  Win32Utils::CheckRun(enable ? "bcdedit /set bootmenupolicy Legacy" : "bcdedit /set bootmenupolicy Standard");
}

bool MiscController::VerboseStatusEnabled()
{
  auto key = Key::Open(Hive::LocalMachine, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", Access::Read);

  return ValueOrDefault(key, "VerboseStatus", 0);
}

void MiscController::EnableVerboseStatus(bool enable)
{
  auto key = Key::Open(Hive::LocalMachine, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", Access::SetValue);

  key.SetValue("VerboseStatus", enable, ValueType::Dword);
}

bool MiscController::CFGEnabled()
{
#ifndef _M_X64
  Wow64RedirectionRAII wow64raii(true);
#endif

  auto output = Win32Utils::CheckRun("powershell -NoProfile (Get-ProcessMitigation -System  -WarningAction SilentlyContinue).Cfg.Enable");
  
  std::string content = output.first;
  Clean(content);

  if (Win32Utils::IEquals(content, "notset") || Win32Utils::IEquals(content, "off"))
  {
    return false;
  }
  else if (Win32Utils::IEquals(content, "on"))
  {
    return true;
  }

  std::stringstream error;
  error << "Failed to read CFG value in powershell output.";
  error << " stdout: '" << output.first << "', stderr: '" << output.second << "'";

  throw std::runtime_error(error.str());
}

void MiscController::EnableCFG(bool enable)
{
#ifndef _M_X64
  Wow64RedirectionRAII wow64raii(true);
#endif

  std::stringstream cmd;
  cmd << "powershell -NoProfile Set-ProcessMitigation -System -" << (enable ? "Enable" : "Disable") << " CFG";
  auto output = Win32Utils::CheckRun(cmd.str());
}
