#pragma once

#include <string>
class IController
{
public:
  virtual ~IController() = default;

  virtual void OnDelete() = 0;
  virtual void OnOpen() = 0;
  virtual void Refresh(const std::wstring& filter) = 0;
  virtual const std::wstring& GetItem(size_t row, size_t column) const = 0;
  virtual size_t Rows() const = 0;
  virtual size_t Columns() const = 0;
  virtual std::vector<size_t> GetSelectedRows() const = 0;
};