#pragma once

#include <string>
#include <vector>

template <typename Data>
class ListEntry
{
public:
  ListEntry(const Data& data, const std::vector<std::wstring>& entries);

  const std::wstring& GetEntry(size_t index) const;
  Data& GetData();

private:
  Data _data;
  std::vector<std::wstring> _entries;
};


#include "ListEntry.hxx"