#include <libreg/SyscallWrapper.h>
#include <fstream>
#include <Shlobj.h>
#include <shellapi.h>
#include <map>
#include <set>

#include "ScheduledTasksController.h"
#include "Win32Utils.h"

using libreg::Syscall;
using libreg::ComCall;

ScheduledTasksController::ScheduledTasksController(HWND list)
  : ListController(list)
{
}

void ScheduledTasksController::RefreshImpl()
{
  Win32Utils::InitializeCom();

  CComPtr<ITaskService> service;
  Syscall(
    CoCreateInstance,
    CLSID_TaskScheduler,
    nullptr,
    CLSCTX_INPROC_SERVER,
    IID_ITaskService,
    reinterpret_cast<LPVOID*>(&service.p));

  ComCall(
    "ITaskService::Connect",
    &ITaskService::Connect,
    service.p,
    VARIANT{},
    VARIANT{},
    VARIANT{},
    VARIANT{}
  );

  CComPtr<ITaskFolder> root;
  ComCall(
    "ITaskService::GetFolder",
    &ITaskService::GetFolder,
    service.p,
    BSTR(L"\\"),
    &root.p
  );

  Visit(root);
}

void ScheduledTasksController::DeleteImpl(Entry& row)
{
  auto& task = row.GetData();

  auto name = task->Name();
  ComCall(
    "ITaskFolder::DeleteTask",
    &ITaskFolder::DeleteTask,
    task->Parent(),
    name.data(),
    static_cast<LONG>(0)
  );
}

void ScheduledTasksController::OpenImpl(Entry& row)
{
  std::string tmp = getenv("tmp");

  std::string temp_path(MAX_PATH, '\0');

  GetTempFileNameA(tmp.c_str(), "WinToolSet", 0, temp_path.data());
  temp_path.resize(strlen(temp_path.c_str()));

  temp_path += ".txt";

  std::wofstream file;
  file.exceptions(std::ios_base::failbit); 
  file.open(temp_path);

  file << row.GetData()->XML();
  file.close();

  SHELLEXECUTEINFOA infos = { 0 };
  infos.cbSize = sizeof(infos);
  infos.lpVerb = "edit";
  infos.lpFile = temp_path.data();
  infos.nShow = SW_SHOW;

  Syscall(ShellExecuteExA, &infos);
}

size_t ScheduledTasksController::Columns() const
{
  return 4;
}

void ScheduledTasksController::Visit(ITaskFolder* folder)
{
  // List tasks in folder
  CComPtr<IRegisteredTaskCollection> tasks;
  ComCall(
    "ITaskFolder::GetTasks",
    &ITaskFolder::GetTasks,
    folder,
    static_cast<LONG>(TASK_ENUM_HIDDEN),
    &tasks.p
  );
  
  Visit(tasks.p, folder);

  // List sub-folders
  CComPtr<ITaskFolderCollection> folders;
  ComCall(
    "ITaskFolder::GetFolders",
    &ITaskFolder::GetFolders,
    folder,
    static_cast<LONG>(0),
    &folders.p
  );

  Visit(folders.p);
}

void ScheduledTasksController::Visit(IRegisteredTaskCollection* tasks, ITaskFolder* folder)
{
  LONG count = 0;
  ComCall(
    "IRegisteredTaskCollection::get_Count",
    &IRegisteredTaskCollection::get_Count,
    tasks,
    &count
  );

  for (LONG i = 1; i <= count; i++)
  {
    CComPtr<IRegisteredTask> task;

    VARIANT index;
    index.vt = VT_UINT;
    index.uintVal = i;

    ComCall(
      "IRegisteredTaskCollection::get_Item",
      &IRegisteredTaskCollection::get_Item,
      tasks,
      index,
      &task.p
    );
    Visit(task.p, folder);
  }
}

void ScheduledTasksController::Visit(ITaskFolderCollection* folders)
{
  LONG count = 0;
  ComCall(
    "IRegisteredTaskCollection::get_Count",
    &ITaskFolderCollection::get_Count,
    folders,
    &count
  );

  for (LONG i = 1; i <= count; i++)
  {
    CComPtr<ITaskFolder> folder;

    VARIANT index;
    index.vt = VT_UINT;
    index.uintVal = i;

    ComCall(
      "ITaskFolderCollection::get_Item",
      &ITaskFolderCollection::get_Item,
      folders,
      index,
      &folder.p
    );
    Visit(folder.p);
  }
}

std::wstring ScheduledTasksController::FormatTriggers(const std::vector<TASK_TRIGGER_TYPE2>& triggers)
{
  static const std::map<TASK_TRIGGER_TYPE2, std::wstring> mappings
  {
    {TASK_TRIGGER_EVENT, L"Event"},
    {TASK_TRIGGER_TIME, L"Time"},
    {TASK_TRIGGER_DAILY, L"Daily"},
    {TASK_TRIGGER_WEEKLY, L"Weekly"},
    {TASK_TRIGGER_MONTHLY, L"Monthly"},
    {TASK_TRIGGER_MONTHLYDOW, L"MonthlyDow"},
    {TASK_TRIGGER_IDLE, L"Idle"},
    {TASK_TRIGGER_REGISTRATION, L"Task registration"},
    {TASK_TRIGGER_BOOT, L"Boot"},
    {TASK_TRIGGER_LOGON, L"Login"},
    {TASK_TRIGGER_SESSION_STATE_CHANGE, L"Session change"},
    {TASK_TRIGGER_CUSTOM_TRIGGER_01, L"Custom trigger"}
  };

  std::set<TASK_TRIGGER_TYPE2> unique_triggers(triggers.begin(), triggers.end());
  std::wstringstream str;

  for (const auto& e : unique_triggers)
  {
    if (str.rdbuf()->in_avail() > 0)
    {
      str << L",";
    }

    auto it = mappings.find(e);
    str << (it != mappings.end() ? it->second : L"Unknown (" + std::to_wstring(e) + L")");
  }

  return str.str();
}

void ScheduledTasksController::Visit(IRegisteredTask* ptr, ITaskFolder* parent)
{
  auto task = std::make_shared<ScheduledTask>(ptr, CComPtr<ITaskFolder>(parent));

  auto triggers = task->Triggers();
  auto pred = [&](const auto& e)
  {
    return std::find(triggers.begin(), triggers.end(), e) != triggers.end();
  };

  auto name = (task->Enabled() ? L"" : L"[Disabled] ") + task->Name();

  _rows.emplace_back(Entry(task, { name, task->Path(), task->Action(), FormatTriggers(triggers)}));
}