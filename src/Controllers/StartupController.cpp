#include <Windows.h>
#include <shellapi.h>
#include <Shlobj.h>
#include <sstream>
#include <filesystem>
#include <libreg/SyscallWrapper.h>
#include "StartupController.h"
#include "Win32Utils.h"

using namespace libreg;

static const std::vector<std::pair<Hive, std::wstring>> sources
{
  {Hive::LocalMachine, L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"},
  {Hive::CurrentUser, L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"},
  {Hive::Users, L".DEFAULT\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"}
};

StartupController::StartupController(HWND list) : ListController(list)
{
}

void StartupController::RefreshImpl()
{
  for (const auto &e : sources)
  {
    CollectEntries(e.first, e.second, true);

    // Only HKLM is redirected: 
    // https://docs.microsoft.com/en-us/windows/desktop/winprog64/shared-registry-keys
    if (e.first == Hive::LocalMachine)
    {
      CollectEntries(e.first, e.second, false);
    }
  }

  CollectEntries(CSIDL_STARTUP, L"User startup folder");
  CollectEntries(CSIDL_COMMON_STARTUP, L"Shared startup folder");
}

void StartupController::CreateRegistryEntry(libreg::Hive hive,
  libreg::Wow64Access wow64,
  const std::wstring& name,
  const std::wstring& cmd)
{
  auto pred = [&](const auto &e)
  {
    return e.first == hive;
  };

  auto source = std::find_if(sources.begin(), sources.end(), pred);
  assert(source != sources.end());

  auto key = Key::Open(hive, source->second, Access::AllAccess| wow64);
  auto values = key.Values();

  auto eq_pred = [&](const auto &e)
  {
    return e.first.Value() == name;
  };

  if (std::find_if(values.begin(), values.end(), eq_pred) != values.end())
  {
    std::stringstream str;
    str << "Value '" << name << "' already exists under: '" << hive << source->second << "'";
    throw std::runtime_error(str.str());
  }

  key.SetValue(name, cmd, ValueType::Sz);
}

void StartupController::CreateStatupEntry(int csidl, const std::wstring& name, const std::wstring& cmd)
{
  auto path = Win32Utils::GetCSIDLPath(csidl) + L"\\" + name + L".lnk";

  if (std::filesystem::exists(path))
  {
    std::stringstream str;
    str << "File '" << path << "' already exists";
    throw std::runtime_error(str.str());
  }

  Win32Utils::CreateShortcut(path, cmd);
}

void StartupController::CollectEntries(Hive hive, const std::wstring& path, bool wow64)
{
  auto flags = Access::AllAccess | (wow64 ? Wow64Access::Key64Bits : Wow64Access::Key32Bits);

  auto key = Key::Open(hive, path, flags);

  for (const auto &e : key.Values())
  {
    if (e.second != ValueType::Sz && e.second != ValueType::Expand_sz)
    {
      CreateEntry(key, e.first.Value(), L"[Invalid entry type]", hive, wow64);
    }
    else
    {
      CreateEntry(key, e.first.Value(), key.GetValue<MultiString>(e.first, e.second).Value(), hive, wow64);
    }
  }
}

void StartupController::CollectEntries(int csidl, const std::wstring& type)
{
  auto path = Win32Utils::GetCSIDLPath(csidl);

  for (const auto &e : std::filesystem::directory_iterator(path))
  {
    if (e.path().has_extension() && Win32Utils::IEquals(e.path().extension().string(), ".lnk"))
    {
      _rows.emplace_back(Entry({ e.path().wstring() }, { e.path().filename().wstring(), type, Win32Utils::ReadShortcutPath(e.path().wstring())}));
    }
    else if (!Win32Utils::IEquals(e.path().filename().string(), "desktop.ini")) // If file is not a shortcut, the file itself is ran
    {
      _rows.emplace_back(Entry({ e.path().wstring() }, { e.path().filename().wstring(), type, e.path().wstring() }));
    }
  }
}

void StartupController::CreateEntry(const Key& key, const std::wstring& name, const std::wstring& cmd, libreg::Hive hive, bool wow64)
{
  std::wstringstream type;
  type << "Registry: " << hive << (wow64 ? "X64" : "X86");

  _rows.emplace_back(Entry({ std::make_pair(key, name) }, { name, type.str(), cmd}));
}

void StartupController::OpenExplorer(const std::wstring& path)
{
  std::filesystem::path fspath(path);
  auto folder = fspath.parent_path().wstring();

  Win32Utils::InitializeCom();
  SHELLEXECUTEINFO infos = { 0 };
  infos.cbSize = sizeof(infos);
  infos.fMask = SEE_MASK_FLAG_NO_UI;
  infos.lpVerb = L"open";
  infos.lpFile = folder.c_str();
  infos.nShow = SW_SHOW;

  Syscall(ShellExecuteEx, &infos);
}

void StartupController::DeleteRegistryValue(libreg::Key& key, const std::wstring& value)
{
  key.DeleteValue(value);
}

void StartupController::DeleteStartupFolderEntry(const std::wstring& path)
{
  std::filesystem::remove(path);
}

void StartupController::DeleteImpl(Entry& row)
{
  if (row.GetData().index() == 0)
  {
    auto value = std::get<std::pair<libreg::Key, std::wstring>>(row.GetData());

    DeleteRegistryValue(value.first, value.second);
  }
  else
  {
    assert(row.GetData().index() == 1);

    DeleteStartupFolderEntry(std::get<std::wstring>(row.GetData()));
  }
}

void StartupController::OpenImpl(Entry& row)
{
  if (row.GetData().index() == 0)
  {
    auto key = std::get<std::pair<libreg::Key, std::wstring>>(row.GetData()).first;

    Win32Utils::OpenRegedit(key);
  }
  else
  {
    assert(row.GetData().index() == 1);

    OpenExplorer(std::get<std::wstring>(row.GetData()));
  }
}

size_t StartupController::Columns() const
{
  return 3;
}



