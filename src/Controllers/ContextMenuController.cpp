#include <libreg/KeyNotFoundException.h>
#include <libreg/ValueNotFoundException.h>
#include <libreg/AccessDeniedException.h>
#include "ContextMenuController.h"
#include "Win32Utils.h"

using namespace libreg;

ContextMenuController::ContextMenuController(HWND list)
  : ListController<ListEntry<libreg::Key>>(list)
{
}

void ContextMenuController::RefreshImpl()
{
  if (!_root.has_value())
  {
    return;
  }
  Visit(L"shell", &ContextMenuController::VisitShellEntry);
  Visit(L"shellex\\ContextMenuHandlers", &ContextMenuController::VisitShellExContextMenuEntry);
  Visit(L"shellex\\PropertySheetHandlers", &ContextMenuController::VisitShellExContextMenuEntry);
  Visit(L"background\\shell", &ContextMenuController::VisitShellEntry);
  Visit(L"background\\shellex\\PropertySheetHandlers", &ContextMenuController::VisitShellExContextMenuEntry);
  Visit(L"background\\shellex\\PropertySheetHandlers", &ContextMenuController::VisitShellExContextMenuEntry);
}

void ContextMenuController::CreateEntry(const std::wstring& name, const std::wstring& display_name, const std::wstring& cmd)
{
  auto key = _root.value().CreateSubKey(L"shell\\" + name);

  key.SetValue("", display_name, ValueType::Sz);

  key.CreateSubKey("command").SetValue("", cmd, ValueType::Sz);
}

bool ContextMenuController::SetRoot(const std::wstring& root)
{
  try
  {
    _root = Key::Open(Hive::Root, root, Access::AllAccess);

    return true;
  }
  catch (const KeyNotFoundException&)
  {
    return false;
  }
  catch (const AccessDeniedException&)
  {
    _root = Key::Open(Hive::Root, root, Access::Read);
  }
}

void ContextMenuController::DeleteImpl(Entry& row)
{
  const auto& data = row.GetData();
  auto writable_key = Key::Open(data.Hive(), data.Path(), Access::AllAccess);

  writable_key.Delete();
}

void ContextMenuController::OpenImpl(Entry& row)
{
  Win32Utils::OpenRegedit(row.GetData());
}


size_t ContextMenuController::Columns() const
{
  return 3;
}

void ContextMenuController::Visit(
  const std::wstring& path,
  const std::function<void(ContextMenuController*, const libreg::Key&)>& routine)
{
  std::optional<libreg::Key> key;

  try
  {
    key = _root.value().OpenSubKey(path, Access::Read);
  }
  catch (const KeyNotFoundException)
  {
    return;
  }

  for (const auto &e : key.value().SubKeys(Access::Read))
  {
    routine(this, e);
  }
}

void ContextMenuController::VisitShellEntry(const libreg::Key& key)
{
  std::wstring command_str;

  try
  {
    auto command = key.OpenSubKey("command", Access::Read);

    command_str = command.GetValue<MultiString>("", ValueType::None).Value();
  }
  catch (const KeyNotFoundException&)
  {
    command_str = L"[Empty]";
  }
  catch (const ValueNotFoundException&)
  {
    command_str = L"[Empty]";
  }

  AddEntry(key, command_str);
}

void ContextMenuController::VisitShellExContextMenuEntry(const libreg::Key& key)
{
  std::wstring command_str;

  try
  {
    command_str = key.GetValue<MultiString>("", ValueType::None).Value();
  }
  catch (const ValueNotFoundException&)
  {
    command_str = L"[Empty]";
  }

  // If the value contains a clsid, try to fetch its display name
  if (!command_str.empty() && command_str.front() == '{' && command_str.back() == '}')
  {
    command_str += L" (" + Win32Utils::GetCLSIDDisplayName(command_str) + L")";
  }

  AddEntry(key, command_str);

}

void ContextMenuController::AddEntry(const Key& key, const std::wstring& command)
{
  auto parent = key.Path().Value();
  auto slash = parent.find_last_of('/');

  if (slash != std::wstring::npos && slash != 0)
  {
    parent = parent.substr(0, slash - 1);
  }

  _rows.emplace_back(Entry{ key, {key.Name().Value(), parent, command} });
}