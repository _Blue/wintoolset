#include <Shlobj.h>
#include <shellapi.h>
#include <fstream>
#include <streambuf>
#include <map>
#include <libreg/SyscallWrapper.h>

#include "ContextMenuSendToController.h"
#include "Win32Utils.h"

using libreg::Syscall;

ContextMenuSendToController::ContextMenuSendToController(HWND list)
  : ListController<Entry>(list)
{
}

void ContextMenuSendToController::RefreshImpl()
{
  auto path = Win32Utils::GetCSIDLPath(CSIDL_SENDTO);

  for (const auto &e : std::filesystem::directory_iterator(path))
  {
    if (e.path().has_extension() && Win32Utils::IEquals(e.path().extension().string(), ".lnk"))
    {
      _rows.emplace_back(
        Entry({ e.path() },
          { e.path().filename().wstring(), L"Shortcut", Win32Utils::ReadShortcutPath(e.path().wstring()) }));
    }
    else if (!Win32Utils::IEquals(e.path().filename().string(), "desktop.ini"))
    {
      VisitSpecialEntry(e.path());
    }
  }
}

void ContextMenuSendToController::CreateEntry(
  const std::wstring& name, 
  size_t index,
  const std::wstring& target)
{
  assert(index < Types().size());

  std::wstringstream path;
  path << Win32Utils::GetCSIDLPath(CSIDL_SENDTO);
  path << L"\\" << name;
  path << std::get<0>(Types()[index]);

  if (index == 0)
  {
    Win32Utils::CreateShortcut(path.str(), target);
  }
  else
  {
    std::wofstream file;
    file.exceptions(~std::ios::goodbit);
    file.open(path.str(), std::ios::binary);

    file << target;
  }
}

void ContextMenuSendToController::DeleteImpl(Entry& row)
{
  std::filesystem::remove(row.GetData());
}

void ContextMenuSendToController::OpenImpl(Entry& row)
{
  auto folder = row.GetData().parent_path().wstring();

  Win32Utils::InitializeCom();
  SHELLEXECUTEINFO infos = { 0 };
  infos.cbSize = sizeof(infos);
  infos.fMask = SEE_MASK_FLAG_NO_UI;
  infos.lpVerb = L"open";
  infos.lpFile = folder.c_str();
  infos.nShow = SW_SHOW;

  Syscall(ShellExecuteEx, &infos);
}

size_t ContextMenuSendToController::Columns() const
{
  return 3;
}

void ContextMenuSendToController::VisitSpecialEntry(const std::filesystem::path& path)
{
  std::ifstream file(path);
  if (!file)
  {
    throw std::runtime_error("Failed to open file: " + path.string());
  }

  std::string content({file}, std::istreambuf_iterator<char>());

  auto extension = path.extension().wstring();
  std::transform(extension.begin(), extension.end(), extension.begin(), towlower);

  auto pred = [&](const auto& e)
  {
    return std::get<0>(e) == extension;
  };

  auto it = std::find_if(Types().begin(), Types().end(), pred);
  const auto& type = it != Types().end() ? std::get<1>(*it) : L"Unknown";

  _rows.emplace_back(Entry({ path }, { path.filename().wstring(), type, Win32Utils::Convert(content) }));
}

const std::vector<std::tuple<std::wstring, std::wstring, std::wstring>>& ContextMenuSendToController::Types()
{
  static const std::vector<std::tuple<std::wstring, std::wstring, std::wstring>> types
  {
    {L".lnk", L"Shortcut", L""},
    {L".ZFSendToTarget", L"Compressed folder", L"Zip"},
    {L".DeskLink", L"Desktop", L"desktop"},
    {L".mydocs", L"My documents", L""},
    {L".MAPIMail", L"Email", L"Mail"},
  };

  return types;
}