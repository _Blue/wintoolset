#include <libreg/SyscallWrapper.h>
#include <libreg/Key.h>
#include <libreg/KeyNotFoundException.h>
#include <libreg/ValueNotFoundException.h>
#include <libreg/IncompatibleValueTypeException.h>
#include "ScheduledTask.h"
#include "Win32Utils.h"

using libreg::ComCall;

ScheduledTask::ScheduledTask(CComPtr<IRegisteredTask>&& task, const CComPtr<ITaskFolder>& parent)
  : _task(task), _parent(parent)
{
  ComCall(
    "&IRegisteredTask::get_Definition",
    &IRegisteredTask::get_Definition,
    _task.p,
    &_definition.p);

  ComCall(
    "&ITaskDefinition::get_Triggers",
    &ITaskDefinition::get_Triggers,
    _definition.p,
    &_triggers.p);

  ComCall(
    "&ITaskDefinition::get_Actions",
    &ITaskDefinition::get_Actions,
    _definition.p,
    &_actions.p);
}

std::wstring ScheduledTask::Name()
{
  BSTR name = { 0 };
  ComCall(
    "IRegisteredTask::get_Name",
    &IRegisteredTask::get_Name,
    _task.p,
    &name
  );

  std::wstring result(name);
  SysFreeString(name);

  return result;
}

std::wstring ScheduledTask::Path()
{
  BSTR path = { 0 };
  ComCall(
    "IRegisteredTask::get_Path",
    &IRegisteredTask::get_Path,
    _task.p,
    &path
  );

  std::wstring result(path);
  SysFreeString(path);

  return result;
}

std::wstring ScheduledTask::XML()
{
  BSTR xml;

  ComCall(
    "IRegisteredTask::get_Xml",
    &IRegisteredTask::get_Xml,
    _task.p,
    &xml
  );

  std::wstring result(xml);
  SysFreeString(xml);

  return result;
}

bool ScheduledTask::Enabled()
{
  VARIANT_BOOL enabled = false;
  ComCall(
    "IRegisteredTask::get_Enabled",
    &IRegisteredTask::get_Enabled,
    _task.p,
    &enabled
  );

  return enabled;
}

bool ScheduledTask::HasTrigger(const std::vector<TASK_TRIGGER_TYPE2>& types)
{
  // Collection index starts at 1
  LONG count = 0;
  ComCall(
    "&ITriggerCollection::get_Count",
    &ITriggerCollection::get_Count,
    _triggers.p,
    &count
  );

  for (long i = 1; i <= count; i++)
  {
    CComPtr<ITrigger> trigger;

    VARIANT index;
    index.vt = VT_UINT;
    index.uintVal = i;

    ComCall(
      "&ITriggerCollection::get_Item",
      &ITriggerCollection::get_Item,
      _triggers.p,
      i,
      &trigger.p
    );

    VARIANT_BOOL enabled;
    ComCall(
      "&ITrigger::get_Enabled",
      &ITrigger::get_Enabled,
      trigger.p,
      &enabled
    );

    if (!enabled) // if trigger is disabled, don't consider it part of the task
    {
      continue;
    }

    TASK_TRIGGER_TYPE2 type;
    ComCall(
      "&ITrigger::get_Type",
      &ITrigger::get_Type,
      trigger.p,
      &type
    );

    if (std::find(types.begin(), types.end(), type) != types.end())
    {
      return true;
    }
  }

  return false;
}

std::vector<TASK_TRIGGER_TYPE2> ScheduledTask::Triggers()
{
  std::vector<TASK_TRIGGER_TYPE2> output;

  // Collection index starts at 1
  LONG count = 0;
  ComCall(
    "&ITriggerCollection::get_Count",
    &ITriggerCollection::get_Count,
    _triggers.p,
    &count
  );

  for (long i = 1; i <= count; i++)
  {
    CComPtr<ITrigger> trigger;

    VARIANT index;
    index.vt = VT_UINT;
    index.uintVal = i;

    ComCall(
      "&ITriggerCollection::get_Item",
      &ITriggerCollection::get_Item,
      _triggers.p,
      i,
      &trigger.p
    );

    VARIANT_BOOL enabled;
    ComCall(
      "&ITrigger::get_Enabled",
      &ITrigger::get_Enabled,
      trigger.p,
      &enabled
    );

    if (!enabled) // if trigger is disabled, don't consider it part of the task
    {
      continue;
    }

    TASK_TRIGGER_TYPE2 type;
    ComCall(
      "&ITrigger::get_Type",
      &ITrigger::get_Type,
      trigger.p,
      &type
    );

    output.emplace_back(type);
  }

  return output;
}

std::wstring ScheduledTask::Action()
{
  LONG count = 0;
  ComCall(
    "&IActionCollection::get_Count",
    &IActionCollection::get_Count,
    _actions.p,
    &count
  );

  std::vector<CComPtr<IAction>> actions;

  for (long i = 1; i <= count; i++)
  {
    VARIANT index;
    index.vt = VT_UINT;
    index.uintVal = i;

    CComPtr<IAction> action;
    ComCall(
      "&IActionCollection::get_Item",
      &IActionCollection::get_Item,
      _actions.p,
      i,
      &action.p
    );

    actions.emplace_back(std::move(action));
  }

  if (actions.size() == 1)
  {
    return FormatAction(actions.front());
  }

  std::wstringstream str;
  str << "[";

  for (size_t i = 0; i < actions.size(); i++)
  {
    if (i != 0)
    {
      str << ",";
    }
    str << FormatAction(actions[i]);
  }

  str << "]";

  return str.str();
}

ITaskFolder* ScheduledTask::Parent()
{
  return _parent;
}

std::wstring ScheduledTask::FormatAction(IAction* action)
{
  TASK_ACTION_TYPE type;
  ComCall(
    "IAction::get_Type",
    &IAction::get_Type,
    action,
    &type
  );

  if (type == TASK_ACTION_EXEC)
  {
    return FormatTranslatedAction(reinterpret_cast<IExecAction*>(action));
  }
  else if (type == TASK_ACTION_SEND_EMAIL)
  {
    return FormatTranslatedAction(reinterpret_cast<IEmailAction*>(action));
  }
  else if (type == TASK_ACTION_SHOW_MESSAGE)
  {
    return FormatTranslatedAction(reinterpret_cast<IShowMessageAction*>(action));
  }
  else if (type == TASK_ACTION_COM_HANDLER)
  {
    return FormatTranslatedAction(reinterpret_cast<IComHandlerAction*>(action));
  }

  throw std::runtime_error("Unknown action type: " + std::to_string(type));
}

std::wstring ScheduledTask::FormatTranslatedAction(IComHandlerAction* action)
{
  BSTR clsid = nullptr;
  ComCall(
    "IComHandlerAction::get_ClassId",
    &IComHandlerAction::get_ClassId,
    action,
    &clsid
  );

  std::wstring result(clsid);
  SysFreeString(clsid);

  // Attempt to find a CLSID display name
  std::wstring display_name = Win32Utils::GetCLSIDDisplayName(result);

  return L"Com action: '" + result + L"' (" + display_name + L")";
}

std::wstring ScheduledTask::FormatTranslatedAction(IExecAction* action)
{
  BSTR path = nullptr;
  ComCall(
    "IExecAction::get_Path",
    &IExecAction::get_Path,
    action,
    &path
  );

  std::wstring result(path);
  SysFreeString(path);

  BSTR args = nullptr;
  ComCall(
    "IExecAction::get_Arguments",
    &IExecAction::get_Arguments,
    action,
    &args
  );

  auto output = result + L" " + (args != nullptr ? args : L"");
  SysFreeString(args);

  return output;
}

std::wstring ScheduledTask::FormatTranslatedAction(IEmailAction* action)
{
  BSTR to = nullptr;
  ComCall(
    "IExecAction::get_To",
    &IEmailAction::get_To,
    action,
    &to
  );

  std::wstring result;
  if (to == nullptr)
  {
    result = L"(null)";
  }
  else
  {
    result = to;
  }
  SysFreeString(to);

  return L"Email to: '" + result + L"'";
}

std::wstring ScheduledTask::FormatTranslatedAction(IShowMessageAction * action)
{
  BSTR body = nullptr;
  ComCall(
    "IShowMessageAction::get_MessageBody",
    &IShowMessageAction::get_MessageBody,
    action,
    &body
  );

  std::wstring result;
  if (body == nullptr)
  {
    result = L"(null)";
  }
  else
  {
    result = body;
  }
  SysFreeString(body);

  return L"Show message: '" + result + L"'";
}
