#include <libreg/Key.h>
#include <filesystem>

#include "ListEntry.h"
#include "Controllers\ListController.h"

#pragma once

class ContextMenuSendToController : public ListController<ListEntry<std::filesystem::path>>
{
public:
  ContextMenuSendToController(HWND list);
  ContextMenuSendToController(const ContextMenuSendToController&) = delete;

  const ContextMenuSendToController& operator=(const ContextMenuSendToController&) = delete;

  virtual void RefreshImpl() override;
  void CreateEntry(const std::wstring& name, size_t type, const std::wstring& target);

  static const std::vector<std::tuple<std::wstring, std::wstring, std::wstring>>& Types();

protected:
  virtual void DeleteImpl(Entry& row) override;
  virtual void OpenImpl(Entry& row) override;
  virtual size_t Columns() const override;

private:
  void VisitSpecialEntry(const std::filesystem::path& path);
};
