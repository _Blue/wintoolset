#pragma once

#include <taskschd.h>
#include <ole2.h>
#include <atlbase.h>
#include <atlcom.h>
#include <string>
#include <vector>

class ScheduledTask
{
public:
  ScheduledTask(CComPtr<IRegisteredTask>&& task, const CComPtr<ITaskFolder>& parent);
  ScheduledTask(const ScheduledTask&) = delete;
  const ScheduledTask& operator=(const ScheduledTask&) = delete;

  std::wstring Name();
  std::wstring Path();
  std::wstring XML();
  bool Enabled();

  bool HasTrigger(const std::vector<TASK_TRIGGER_TYPE2>& type);
  std::vector<TASK_TRIGGER_TYPE2> Triggers();

  std::wstring Action();
  ITaskFolder* Parent();

private:
  std::wstring FormatAction(IAction* action);

  std::wstring FormatTranslatedAction(IComHandlerAction* action);
  std::wstring FormatTranslatedAction(IExecAction* action);
  std::wstring FormatTranslatedAction(IEmailAction* action);
  std::wstring FormatTranslatedAction(IShowMessageAction* action);

  CComPtr<ITaskDefinition> _definition;
  CComPtr<IRegisteredTask> _task;
  CComPtr<ITriggerCollection> _triggers;
  CComPtr<IActionCollection> _actions;
  CComPtr<ITaskFolder> _parent;

};