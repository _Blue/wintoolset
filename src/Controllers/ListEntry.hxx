#pragma once
#include <cassert>
#include "ListEntry.h"

template <typename Data>
inline ListEntry<Data>::ListEntry(const Data& data, const std::vector<std::wstring>& entries)
  : _data(data), _entries(entries)
{
}

template <typename Data>
inline const std::wstring& ListEntry<Data>::GetEntry(size_t index) const
{
  assert(index < _entries.size());

  return _entries[index];
}

template <typename Data>
inline Data& ListEntry<Data>::GetData()
{
  return _data;
}