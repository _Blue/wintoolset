#include <Windows.h>
#include <libreg/SyscallWrapper.h>
#include <libreg/ValueNotFoundException.h>
#include <libreg/KeyNotFoundException.h>
#include <libreg/IncompatibleValueTypeException.h>
#include <Shlobj.h>
#include <ShObjIdl.h>
#include <cassert>
#include <locale>
#include <codecvt>
#include <commdlg.h>
#include <sstream>
#include <string.h>
#include <ole2.h>
#include <atlbase.h>
#include <atlcom.h>
#include <io.h>
#include <fstream>

#include "Win32Utils.h"
#include "res/Resource.h"

using namespace libreg;

static CComModule _com_module;

namespace Win32Utils
{
  static std::pair<CComPtr<IShellLink>, CComPtr<IPersistFile>> BuildPersistFile()
  {
    InitializeCom();

    CComPtr<IShellLink> shell_link;
    CComPtr<IPersistFile> persist_file;

    Syscall(
      CoCreateInstance,
      CLSID_ShellLink,
      nullptr,
      CLSCTX_INPROC_SERVER,
      IID_IShellLinkW,
      reinterpret_cast<LPVOID*>(&shell_link));

    HRESULT(STDMETHODCALLTYPE IShellLink::*query_interface)(const IID&, void**) = &IShellLink::QueryInterface;

    ComCall<IShellLink, const IID&, void**>(
      "IShellLink::QueryInterface",
      query_interface,
      shell_link.p,
      (const IID&)IID_IPersistFile,
      reinterpret_cast<void**>(&persist_file.p));

    return std::make_pair(shell_link, persist_file);
  }

  std::wstring ReadShortcutPath(const std::wstring& file)
  {
    auto[shell_link, persist_file] = BuildPersistFile();

    ComCall(
      "IPersistFile::Load",
      &IPersistFile::Load,
      persist_file.p,
      file.c_str(),
      static_cast<DWORD>(STGM_READ)
    );

    WIN32_FIND_DATA find_data = { 0 };
    std::wstring path(MAX_PATH, '\0');

    ComCall(
      "IShellLink::GetPath",
      &IShellLink::GetPath,
      shell_link.p,
      path.data(),
      static_cast<int>(path.size()),
      &find_data,
      static_cast<DWORD>(SLGP_SHORTPATH)
    );

    path.resize(wcslen(path.c_str()));
    return path;
  }

  void CreateShortcut(const std::wstring & path, const std::wstring& target)
  {
    auto[shell_link, persist_file] = BuildPersistFile();

    ComCall(
      "IShellLink::SetPath",
      &IShellLink::SetPath,
      shell_link.p,
      target.c_str()
    );

    ComCall(
      "IShellLink::SetDescription",
      &IShellLink::SetDescription,
      shell_link.p,
      L""
      );

    ComCall(
      "IPersistFile::Save",
      &IPersistFile::Save,
      persist_file.p,
      path.c_str(),
      TRUE
    );
  }

  void InitializeCom()
  {
    static bool initialized = false;

    if (initialized)
    {
      return;
    }

    Syscall(
      CoInitialize,
      nullptr
    );

    initialized = true;
  }

  std::wstring GetWindowText(HWND window)
  {
    size_t size = GetWindowTextLength(window);

    std::wstring text(size, '\0');

    size = ::GetWindowText(window, text.data(), static_cast<int>(text.size() + 1));

    assert(size == text.size());

    return text;
  }

  static void LogImpl(HWND window, const std::wstring& text)
  {
    HWND console = GetDlgItem(window, IDC_CONSOLE);
    assert(console != nullptr);

    int index = GetWindowTextLength(console);

    HWND focus = GetFocus();
    SendMessage(console, EM_SETSEL, index, index);
    SendMessage(console, EM_REPLACESEL, 0, reinterpret_cast<LPARAM>(text.c_str()));
    SetFocus(focus);
  }

  void OpenRegedit(const libreg::Key& location)
  {
    // Set the 'LastKey' value for regedit to open at the right location
    std::wstringstream path;
    path << L"Computer\\" << location.Hive() << L"\\" << location.Path().Value();

    auto key = libreg::Key::OpenOrCreate(Hive::CurrentUser, "Software\\Microsoft\\Windows\\CurrentVersion\\Applets\\Regedit", Access::AllAccess);
    key.SetValue("LastKey", path.str(), ValueType::Sz);

    // Disable wow64 fs redirection
#ifndef _M_X64
    auto wow64raii = Win32Utils::Wow64RedirectionRAII((location.Access() & Wow64Access::Key64Bits));
#endif 

    // Create regedit process
    STARTUPINFO startup_infos = { 0 };
    PROCESS_INFORMATION process_infos = { 0 };

    std::wstring cmd = L"regedit";
    Syscall(
      CreateProcess,
      nullptr, // lpApplicationName
      const_cast<LPTSTR>(cmd.c_str()), // lpCommandLine
      nullptr, // lpProcessAttributes
      nullptr, // lpThreadAttributes
      false, // bInheritHandles
      0, // dwCreationFlags
      nullptr, // lpEnvironment
      nullptr, // lpCurrentDirectory
      &startup_infos, // lpStartupInfo
      &process_infos // lpProcessInformation
    );
  }

  bool IEquals(const std::string& left, const std::string& right)
  {
    auto pred = [](char l, char r)
    {
      return tolower(l) == tolower(r);
    };

    return std::equal(left.begin(), left.end(), right.begin(), right.end(), pred);
  }

  void Log(HWND window, const std::string& text, bool lf)
  {
    Log(window, Convert(text), lf);
  }

  void Log(HWND window, const std::wstring& text, bool lf)
  {
    if (lf)
    {
      LogImpl(window, L"\r\n" + text);
    }
    else
    {
      LogImpl(window, text);
    }
  }

  void SetErrorState(HWND window, bool set)
  {
    HWND console = GetDlgItem(window, IDC_CONSOLE);
    assert(console != nullptr);

    SetWindowLongPtr(console, GWLP_USERDATA, true);

    Syscall(RedrawWindow, console, nullptr, nullptr, RDW_INVALIDATE | RDW_ERASE);
  }

  std::wstring Convert(const std::string& input)
  {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

    return converter.from_bytes(input);
  }

  std::string Convert(const std::wstring& input)
  {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

    return converter.to_bytes(input);
  }

  std::wstring GetCSIDLPath(int csidl)
  {
    // https://docs.microsoft.com/en-us/windows/desktop/api/shlobj_core/nf-shlobj_core-shgetfolderpatha
    //  A pointer to a null-terminated string of length MAX_PATH which will receive the path.

    std::wstring path(MAX_PATH, '\0');

    Syscall(
      SHGetFolderPath,
      nullptr,
      csidl,
      nullptr,
      SHGFP_TYPE_DEFAULT,
      path.data());

    path.resize(wcslen(path.c_str()));

    return path;
  }

  std::optional<std::wstring> BrowseForExistingFile(
    HWND parent,
    const std::wstring& description)
  {
    std::wstring path(MAX_PATH, '\0');

    OPENFILENAME data = { 0 };
    data.lStructSize = sizeof(OPENFILENAME);
    data.hInstance = nullptr;;
    data.hwndOwner = parent;
    data.lpstrFile = path.data();
    data.nMaxFile = MAX_PATH;
    data.lpstrTitle = description.c_str();

    data.Flags = OFN_FILEMUSTEXIST | OFN_FORCESHOWHIDDEN | OFN_NONETWORKBUTTON | OFN_PATHMUSTEXIST | OFN_EXPLORER;
    if (GetOpenFileName(&data))
    {
      path.resize(wcslen(path.c_str()));
      return path;
    }

    return {};
  }

  int Width(const RECT& rect)
  {
    return rect.right - rect.left;
  }

  int Height(const RECT& rect)
  {
    return rect.bottom - rect.top;
  }

  Wow64RedirectionRAII::Wow64RedirectionRAII(bool change) : _change(change)
  {
    if (change)
    {
      Syscall(
        Wow64DisableWow64FsRedirection,
        &_value // OldValue
      );
    }
  }

  Wow64RedirectionRAII::Wow64RedirectionRAII(Wow64RedirectionRAII && other)
  {
    _value = other._value;
    other._value = nullptr;
  }

  const Wow64RedirectionRAII& Win32Utils::Wow64RedirectionRAII::operator=(Wow64RedirectionRAII && other)
  {
    _value = other._value;
    other._value = nullptr;

    return *this;
  }

  Wow64RedirectionRAII::~Wow64RedirectionRAII()
  {
    if (_change)
    {
      Syscall(
        Wow64RevertWow64FsRedirection,
        &_value // OldValue
      );
    }
  }

  std::pair<Handle<HANDLE>, Handle<HANDLE>> CreatePipe(bool rs, bool ws)
  {
    HANDLE read = nullptr;
    HANDLE write = nullptr;
    SECURITY_ATTRIBUTES sa = { sizeof(SECURITY_ATTRIBUTES), nullptr, true };

    Syscall(::CreatePipe, &read, &write, &sa, 0);

    if (rs)
    {
      Syscall(SetHandleInformation, read, HANDLE_FLAG_INHERIT, 0);
    }
    if (ws)
    {
      Syscall(SetHandleInformation, write, HANDLE_FLAG_INHERIT, 0);
    }

    return{ read, write };
  }

  static FILE* FileFromHandle(HANDLE  hFile, const char* mode)
  {
    int fd = _open_osfhandle(reinterpret_cast<intptr_t>(hFile), 0);
    if (fd < 0)
    {
      throw std::runtime_error("_open_osfhandle failed, " + std::to_string(GetLastError()));
    }

    FILE *result = _fdopen(fd, mode);
    if (result == nullptr)
    {
      throw std::runtime_error("_fdopen failed, " + std::to_string(GetLastError()));
    }
    return result;
  }

  std::pair<std::string, std::string> CheckRun(const std::string& cmd)
  {
    // Create the pipes
    auto stdout_pipe = CreatePipe(true, false);
    auto stderr_pipe = CreatePipe(true, false);

    // Create std streams out of Win Handles
    std::ifstream stdout_str(FileFromHandle(stdout_pipe.first.Get(), "r"));
    std::ifstream stderr_str(FileFromHandle(stderr_pipe.first.Get(), "r"));

    // Make sure handles won't be closed twice
    stdout_pipe.first.Detach();
    stderr_pipe.first.Detach();

    // Start process
    PROCESS_INFORMATION pi = { 0 };
    STARTUPINFOA si = { 0 };
    si.cb = sizeof(STARTUPINFO);
    si.hStdOutput = stdout_pipe.second;
    si.hStdError = stderr_pipe.second;
    si.wShowWindow = SW_HIDE;
    si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;

    Syscall(CreateProcessA,
      nullptr, // lpApplicationName
      const_cast<char*>(cmd.data()), // lpCommandLine
      nullptr, // lpProcessAttributes
      nullptr, // lpThreadAttributes
      true, // bInheritHandles
      0, // dwCreationFlags
      nullptr, // lpEnvironment
      nullptr, // lpCurrentDirectory
      &si, // lpStartupInfo
      &pi // lpProcessInformation
    );

    CloseHandle(pi.hThread);

    Handle<HANDLE> process(pi.hProcess);

    // Wait for process to finish
    SyscallWithExpectedReturn(WaitForSingleObject, WAIT_OBJECT_0, process.Get(), INFINITE);

    // Verify exit code
    DWORD dwExitCode = 0;
    Syscall(GetExitCodeProcess, process.Get(), &dwExitCode);

    // Close writing ends
    stdout_pipe.second.Close();
    stderr_pipe.second.Close();

    // Read stdout and stderr
    auto output = std::make_pair<std::string, std::string>(
      { std::istreambuf_iterator<char>(stdout_str), {} },
      { std::istreambuf_iterator<char>(stderr_str), {} });

    if (dwExitCode != 0)
    {
      std::stringstream error;
      error << "subprocess '" + cmd + "'failed with error code: ";
      error << dwExitCode << ", stdout: '" << output.first;
      error << "', stderr: '" << output.second << "'";

      throw std::runtime_error(error.str());
    }

    return output;
  }

  std::string GetErrorMessage(DWORD code)
  {
    LPSTR messageBuffer = nullptr;

    DWORD size = FormatMessageA(
      FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
      nullptr,
      code,
      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
      reinterpret_cast<LPSTR>(&messageBuffer),
      0,
      nullptr);

    std::string message(messageBuffer, size);

    LocalFree(messageBuffer);

    return message;
  }

  std::wstring GetCLSIDDisplayName(const std::wstring& clsid)
  {
    try
    {
      auto key = libreg::Key::Open(libreg::Hive::Root, L"CLSID\\" + clsid, libreg::Access::Read);

      return key.GetValue<libreg::MultiString>("").Value();
    }
    catch (const libreg::KeyNotFoundException&)
    {
      return L"key not found";
    }
    catch (const libreg::ValueNotFoundException&)
    {
      return L"Value not found";
    }
    catch (const libreg::IncompatibleValueTypeException&)
    {
      return L"Invalid: bad value type";
    }
  }

  void LogError(HWND window, const libreg::SyscallFailure& error)
  {
    std::stringstream str;
    str << "Error messages: ";

    if (error.ReturnValue() != 0)
    {
      str << "\r\n - Return value: " << Win32Utils::GetErrorMessage(error.ReturnValue());
    }

    if (error.LastError() != 0)
    {
      str << std::endl << "\r\n - GetLastError(): " << Win32Utils::GetErrorMessage(error.ReturnValue());
    }

    Log(window, str.str());
  }

  void LogError(HWND window, const std::string& message, const std::exception& error)
  {
    std::stringstream str;
    str << message << ": " << error.what() << std::endl;

    Log(window, str.str());
    SetErrorState(window);

    auto* syscall_failure = dynamic_cast<const libreg::SyscallFailure*>(&error);
    if (syscall_failure != nullptr)
    {
      LogError(window, *syscall_failure);
    }
  }
}