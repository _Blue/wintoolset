#include <Windows.h>
#include "GUI/MainWindow.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
  _In_opt_ HINSTANCE hPrevInstance,
  _In_ LPWSTR    lpCmdLine,
  _In_ int       nCmdShow)
{
  MainWindow window(hInstance);

  window.Show();

  return 0;
}