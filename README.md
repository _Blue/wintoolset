# WinToolSet

** A tool to customize and audit various hard-to-access Windows settings **

Features:

* Startup programs management
* Scheduled task management (also shows hidden tasks)
* Shell context menu management
* Various miscellanous settings

## Build

```bash
# Clone the repo
$ git clone https://bitbucket.org/_Blue/WinToolSet
$ cd WinToolSet

# Pull nuget packages
$ nuget restore

# Build using msbuild (or VS)
$ msbuild WinToolSet.sln
```

Prebuilt binairies:

* [** X86 binary **](https://vortex.bluecode.fr/wintoolset-X86.exe)
* [** X64 binary **](https://vortex.bluecode.fr/wintoolset-X64.exe)

## Screenshots

### Startup programs management
![1](screenshots/startup.png)

## Scheduled tasks management
![2](screenshots/tasks.png)

## Shell context menu management 
![3](screenshots/context.png)

## Miscellanous settings
![4](screenshots/misc.png)